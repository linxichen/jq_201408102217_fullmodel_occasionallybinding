(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     19728,        611]
NotebookOptionsPosition[     19237,        589]
NotebookOutlinePosition[     19597,        605]
CellTagsIndexPosition[     19554,        602]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"dbar", "=", "0.1148"}], ";", "\[IndentingNewLine]", 
    RowBox[{"kkappa", "=", "0.1460"}], ";", "\[IndentingNewLine]", 
    RowBox[{"ddelta", "=", "0.0250"}], ";", "\[IndentingNewLine]", 
    RowBox[{"aalpha", " ", "=", "0.3600"}], ";", "\[IndentingNewLine]", 
    RowBox[{"bbeta", "=", "0.9825"}], ";", "\[IndentingNewLine]", 
    RowBox[{"xxibar", "=", "0.1634"}], ";", "\[IndentingNewLine]", 
    RowBox[{"ttau", "=", "0.3500"}], ";"}], "*)"}], "\[IndentingNewLine]", 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"f1", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", 
     RowBox[{"llambda", "-", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"2", "*", "kkappa", "*", 
          RowBox[{"(", 
           RowBox[{"d", "-", "dbar"}], ")"}]}]}], ")"}]}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f2", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "bbeta"}], "*", "EMK"}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"llambda", "-", 
         RowBox[{"mmu", "*", "xxi"}]}], ")"}], "/", "c"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f3", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "bbeta"}], "*", "EMB"}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"llambda", "/", "R"}], "-", 
         RowBox[{"mmu", "*", "xxi", "*", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"1", "-", "ttau"}], ")"}], "/", 
           RowBox[{"(", 
            RowBox[{"R", "-", "ttau"}], ")"}]}]}]}], ")"}], "/", "c"}]}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f4", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "bbeta"}], "*", "EMC"}], "+", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "ttau"}], ")"}], "/", 
        RowBox[{"(", 
         RowBox[{"R", "-", "ttau"}], ")"}]}], "/", "c"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f5", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{"aalpha", "*", 
       RowBox[{"c", "/", 
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}]}]}], "-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", 
         RowBox[{"mmu", "/", "llambda"}]}], ")"}], "*", 
       RowBox[{"(", 
        RowBox[{"1", "-", "ttheta"}], ")"}], "*", "z", "*", 
       RowBox[{"k", "^", 
        RowBox[{"(", "ttheta", ")"}]}], "*", 
       RowBox[{"n", "^", 
        RowBox[{"(", 
         RowBox[{"-", "ttheta"}], ")"}]}]}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f6", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", "ddelta"}], ")"}], "*", "k"}], "+", 
      RowBox[{"z", "*", 
       RowBox[{"k", "^", 
        RowBox[{"(", "ttheta", ")"}]}], "*", 
       RowBox[{"n", "^", 
        RowBox[{"(", 
         RowBox[{"1", "-", "ttheta"}], ")"}]}]}], "-", "c", "-", "kplus", "-", 
      RowBox[{"kkappa", "*", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"d", "-", "dbar"}], ")"}], "^", "2"}]}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f7", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{"aalpha", "*", "c", "*", 
       RowBox[{"n", "/", 
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}]}]}], "+", "b", "+", "d", "-", "c", 
      "-", 
      RowBox[{"bplus", "/", "R"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"f8", "[", 
      RowBox[{
      "kplus_", ",", "bplus_", ",", "c_", ",", "d_", ",", "n_", ",", "R_", 
       ",", "mmu_", ",", "llambda_"}], "]"}], " ", ":=", " ", "\n", 
     RowBox[{
      RowBox[{"xxi", "*", "kplus"}], "-", 
      RowBox[{"xxi", "*", "bplus", "*", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "ttau"}], ")"}], "/", 
        RowBox[{"(", 
         RowBox[{"R", "-", "ttau"}], ")"}]}]}], "-", 
      RowBox[{"z", "*", 
       RowBox[{"k", "^", 
        RowBox[{"(", "ttheta", ")"}]}], "*", 
       RowBox[{"n", "^", 
        RowBox[{"(", 
         RowBox[{"1", "-", "ttheta"}], ")"}]}]}]}]}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.616856105187195*^9, 3.6168561278068237`*^9}, 
   3.616858677700781*^9, {3.616858731372314*^9, 3.616858736706415*^9}, {
   3.616858901506166*^9, 3.616858931259357*^9}, {3.616859292883913*^9, 
   3.616859360140462*^9}, {3.6168595785652933`*^9, 3.6168596258346653`*^9}, {
   3.616859682828108*^9, 3.616859704971336*^9}, {3.616859743771875*^9, 
   3.6168598031151333`*^9}, 3.6168598767089148`*^9, {3.61685997823615*^9, 
   3.6168601831999483`*^9}, 3.616860410509399*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"f1", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f2", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f3", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f4", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f5", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f6", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f7", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}], ",", 
     RowBox[{"f8", "[", 
      RowBox[{
      "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
       "mmu", ",", "llambda"}], "]"}]}], "\[IndentingNewLine]", "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{
     "kplus", ",", "bplus", ",", "c", ",", "d", ",", "n", ",", "R", ",", 
      "mmu", ",", "llambda"}], "}"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.616860213450603*^9, 3.6168602823578453`*^9}, {
  3.616860330419567*^9, 3.61686039531481*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", 
     FractionBox[
      RowBox[{"2", " ", "kkappa"}], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"2", " ", 
          RowBox[{"(", 
           RowBox[{"d", "-", "dbar"}], ")"}], " ", "kkappa"}]}], ")"}], "2"]],
      ",", "0", ",", "0", ",", "0", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"llambda", "-", 
        RowBox[{"mmu", " ", "xxi"}]}], 
       SuperscriptBox["c", "2"]]}], ",", "0", ",", "0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox["xxi", "c"]}], ",", 
     FractionBox["1", "c"]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        FractionBox["llambda", "R"], "-", 
        FractionBox[
         RowBox[{"mmu", " ", 
          RowBox[{"(", 
           RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], 
         RowBox[{"R", "-", "ttau"}]]}], 
       SuperscriptBox["c", "2"]]}], ",", "0", ",", "0", ",", 
     FractionBox[
      RowBox[{
       RowBox[{"-", 
        FractionBox["llambda", 
         SuperscriptBox["R", "2"]]}], "+", 
       FractionBox[
        RowBox[{"mmu", " ", 
         RowBox[{"(", 
          RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"R", "-", "ttau"}], ")"}], "2"]]}], "c"], ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], 
       RowBox[{"c", " ", 
        RowBox[{"(", 
         RowBox[{"R", "-", "ttau"}], ")"}]}]]}], ",", 
     FractionBox["1", 
      RowBox[{"c", " ", "R"}]]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"1", "-", "ttau"}], 
       RowBox[{
        SuperscriptBox["c", "2"], " ", 
        RowBox[{"(", 
         RowBox[{"R", "-", "ttau"}], ")"}]}]]}], ",", "0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"1", "-", "ttau"}], 
       RowBox[{"c", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"R", "-", "ttau"}], ")"}], "2"]}]]}], ",", "0", ",", "0"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     FractionBox["aalpha", 
      RowBox[{"1", "-", "n"}]], ",", "0", ",", 
     RowBox[{
      FractionBox[
       RowBox[{"aalpha", " ", "c"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}], "2"]], "+", 
      RowBox[{
       SuperscriptBox["k", "ttheta"], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", 
         FractionBox["mmu", "llambda"]}], ")"}], " ", 
       SuperscriptBox["n", 
        RowBox[{
         RowBox[{"-", "1"}], "-", "ttheta"}]], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "ttheta"}], ")"}], " ", "ttheta", " ", "z"}]}], ",",
      "0", ",", 
     FractionBox[
      RowBox[{
       SuperscriptBox["k", "ttheta"], " ", 
       SuperscriptBox["n", 
        RowBox[{"-", "ttheta"}]], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], "llambda"], ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        SuperscriptBox["k", "ttheta"], " ", "mmu", " ", 
        SuperscriptBox["n", 
         RowBox[{"-", "ttheta"}]], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], 
       SuperscriptBox["llambda", "2"]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "1"}], ",", "0", ",", 
     RowBox[{"-", "1"}], ",", 
     RowBox[{
      RowBox[{"-", "2"}], " ", 
      RowBox[{"(", 
       RowBox[{"d", "-", "dbar"}], ")"}], " ", "kkappa"}], ",", 
     RowBox[{
      SuperscriptBox["k", "ttheta"], " ", 
      SuperscriptBox["n", 
       RowBox[{"-", "ttheta"}]], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], ",", "0", ",", "0", 
     ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", 
      FractionBox["1", "R"]}], ",", 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      FractionBox[
       RowBox[{"aalpha", " ", "n"}], 
       RowBox[{"1", "-", "n"}]]}], ",", "1", ",", 
     RowBox[{
      FractionBox[
       RowBox[{"aalpha", " ", "c"}], 
       RowBox[{"1", "-", "n"}]], "+", 
      FractionBox[
       RowBox[{"aalpha", " ", "c", " ", "n"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}], "2"]]}], ",", 
     FractionBox["bplus", 
      SuperscriptBox["R", "2"]], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"xxi", ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], 
       RowBox[{"R", "-", "ttau"}]]}], ",", "0", ",", "0", ",", 
     RowBox[{
      RowBox[{"-", 
       SuperscriptBox["k", "ttheta"]}], " ", 
      SuperscriptBox["n", 
       RowBox[{"-", "ttheta"}]], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], ",", 
     FractionBox[
      RowBox[{"bplus", " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"R", "-", "ttau"}], ")"}], "2"]], ",", "0", ",", "0"}], 
    "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.616860403047679*^9, 3.61686042182712*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Grid", "[", "%9", "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 TagBox[GridBox[{
    {"0", "0", "0", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"2", " ", "kkappa"}], ")"}], "/", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"2", " ", 
          RowBox[{"(", 
           RowBox[{"d", "-", "dbar"}], ")"}], " ", "kkappa"}]}], ")"}], 
       "2"]}], "0", "0", "0", "1"},
    {"0", "0", 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"llambda", "-", 
         RowBox[{"mmu", " ", "xxi"}]}], ")"}], "/", 
       SuperscriptBox["c", "2"]}]}], "0", "0", "0", 
     RowBox[{"-", 
      FractionBox["xxi", "c"]}], 
     FractionBox["1", "c"]},
    {"0", "0", 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"llambda", "/", "R"}], "-", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"mmu", " ", 
            RowBox[{"(", 
             RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{"R", "-", "ttau"}], ")"}]}]}], ")"}], "/", 
       SuperscriptBox["c", "2"]}]}], "0", "0", 
     RowBox[{
      FractionBox["1", "c"], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", 
         RowBox[{"llambda", "/", 
          SuperscriptBox["R", "2"]}]}], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"mmu", " ", 
           RowBox[{"(", 
            RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], ")"}], "/", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"R", "-", "ttau"}], ")"}], "2"]}]}], ")"}]}], 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], ")"}], "/", 
       RowBox[{"(", 
        RowBox[{"c", " ", 
         RowBox[{"(", 
          RowBox[{"R", "-", "ttau"}], ")"}]}], ")"}]}]}], 
     FractionBox["1", 
      RowBox[{"c", " ", "R"}]]},
    {"0", "0", 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", "ttau"}], ")"}], "/", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["c", "2"], " ", 
         RowBox[{"(", 
          RowBox[{"R", "-", "ttau"}], ")"}]}], ")"}]}]}], "0", "0", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"1", "-", "ttau"}], 
       RowBox[{"c", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"R", "-", "ttau"}], ")"}], "2"]}]]}], "0", "0"},
    {"0", "0", 
     FractionBox["aalpha", 
      RowBox[{"1", "-", "n"}]], "0", 
     RowBox[{
      FractionBox[
       RowBox[{"aalpha", " ", "c"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}], "2"]], "+", 
      RowBox[{
       SuperscriptBox["k", "ttheta"], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", 
         RowBox[{"mmu", "/", "llambda"}]}], ")"}], " ", 
       SuperscriptBox["n", 
        RowBox[{
         RowBox[{"-", "1"}], "-", "ttheta"}]], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "ttheta"}], ")"}], " ", "ttheta", " ", "z"}]}], "0", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["k", "ttheta"], " ", 
        SuperscriptBox["n", 
         RowBox[{"-", "ttheta"}]], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], ")"}], "/", 
      "llambda"}], 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["k", "ttheta"], " ", "mmu", " ", 
         SuperscriptBox["n", 
          RowBox[{"-", "ttheta"}]], " ", 
         RowBox[{"(", 
          RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], ")"}], "/", 
       SuperscriptBox["llambda", "2"]}]}]},
    {
     RowBox[{"-", "1"}], "0", 
     RowBox[{"-", "1"}], 
     RowBox[{
      RowBox[{"-", "2"}], " ", 
      RowBox[{"(", 
       RowBox[{"d", "-", "dbar"}], ")"}], " ", "kkappa"}], 
     RowBox[{
      SuperscriptBox["k", "ttheta"], " ", 
      SuperscriptBox["n", 
       RowBox[{"-", "ttheta"}]], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], "0", "0", "0"},
    {"0", 
     RowBox[{"-", 
      FractionBox["1", "R"]}], 
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      FractionBox[
       RowBox[{"aalpha", " ", "n"}], 
       RowBox[{"1", "-", "n"}]]}], "1", 
     RowBox[{
      FractionBox[
       RowBox[{"aalpha", " ", "c"}], 
       RowBox[{"1", "-", "n"}]], "+", 
      FractionBox[
       RowBox[{"aalpha", " ", "c", " ", "n"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "-", "n"}], ")"}], "2"]]}], 
     FractionBox["bplus", 
      SuperscriptBox["R", "2"]], "0", "0"},
    {"xxi", 
     RowBox[{"-", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], ")"}], "/", 
       RowBox[{"(", 
        RowBox[{"R", "-", "ttau"}], ")"}]}]}], "0", "0", 
     RowBox[{
      RowBox[{"-", 
       SuperscriptBox["k", "ttheta"]}], " ", 
      SuperscriptBox["n", 
       RowBox[{"-", "ttheta"}]], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "ttheta"}], ")"}], " ", "z"}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"bplus", " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "ttau"}], ")"}], " ", "xxi"}], ")"}], "/", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"R", "-", "ttau"}], ")"}], "2"]}], "0", "0"}
   },
   AutoDelete->False,
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
  "Grid"]], "Output",
 CellChangeTimes->{3.616860442889579*^9}]
}, Open  ]]
},
WindowSize->{1276, 1032},
WindowMargins->{{Automatic, -1276}, {Automatic, 0}},
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 5794, 159, 386, "Input"],
Cell[CellGroupData[{
Cell[6376, 183, 1712, 43, 97, "Input"],
Cell[8091, 228, 5484, 170, 236, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13612, 403, 79, 2, 28, "Input"],
Cell[13694, 407, 5527, 179, 479, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
