#ifndef _CONTAINER_
#define _CONTAINER_
////////////////////////////////////////
///
/// Containes (Matrices, Vectors)
/// Taken more or less directly from Numerical Recipes 3rd. Credits to them.
/// IMPORTANT: I use flattened 1-D array to store matrix in column-major.
///            Also I use (i,j) to access the element, instead of [i,j].
///
////////////////////////////////////////

# define throw(message) {printf("ERROR: %s\n in file %s at line %d\n",message,__FILE__,__LINE__); }

template <typename T>
struct Matrix {
	// Data Members
	int n; // # of rows
	int m; // # of columns
	T* data_ptr; // Pointer to where contents of this matrix is stored (in column major fashion!!).

	// Methods
	__host__ __device__ Matrix();
	__host__ __device__ Matrix(int _n, int _m);			// Zero-based array
	__host__ __device__ Matrix(int _n, int _m, const T a);	//Initialize to constant
	__host__ __device__ Matrix(int _n, int _m, const T *a);	// Initialize to array
	__host__ __device__ Matrix(const Matrix &rhs);		// Copy constructor
	__host__ __device__ Matrix & operator=(const Matrix &rhs);	//assignment
	__host__ __device__ inline T* operator[](const int i);	//subscripting: pointer to column i
	__host__ __device__ inline const T* operator[](const int i) const;
	__host__ __device__ inline T & operator()(const int i, const int j);	//subscripting: return value at (i,j), zero-indexing
	__host__ __device__ inline const T & operator()(const int i, const int j) const;
	__host__ __device__ inline T & operator()(const int i);	//subscripting: return value at linear index i, zero-indexing
	__host__ __device__ inline const T & operator()(const int i) const;
	__host__ __device__ Matrix operator*(const Matrix &rhs);
	__host__ __device__ Matrix operator-();
	__host__ __device__ inline int nrows() const;
	__host__ __device__ inline int ncols() const;
	__host__ __device__ inline int size() const;
	__host__ __device__ inline void resize(int newn, int newm); // resize (contents are preserved)
	__host__ __device__ void assign(int newn, int newm, const T &a); // resize and assign a constant value
	__host__ __device__ ~Matrix();
	typedef T value_type; // make T available externally
};

// Default Constructor
template <class T>
__host__ __device__
Matrix<T>::Matrix() {
	n = 1;
	m = 1;
	data_ptr = new T [n*m];
};

template <class T>
__host__ __device__
Matrix<T>::Matrix(int _n, int _m) {
	n = _n; m = _m;
	if ( (_n>0) && (_m>0) ) {
		data_ptr = new T [n*m];
		for (int i=0; i<n*m; i++) data_ptr[i]=0.0;
	} else {
		data_ptr = NULL;
	};
};

template <class T>
__host__ __device__
Matrix<T>::Matrix(int _n, int _m, const T a) {
	n = _n; m = _m;
	if ( (_n>0) && (_m>0) ) {
		data_ptr = new T [n*m];
		for (int i=0; i<n*m; i++) data_ptr[i] = a;
	} else {
		data_ptr = NULL;
	};
};

template <class T>
__host__ __device__
Matrix<T>::Matrix(int _n, int _m, const T*  a) {
	n = _n; m = _m;
	if ( (_n>0) && (_m>0) ) {
		data_ptr = new T [n*m];
		for (int i=0; i<n*m; i++) data_ptr[i] = a[i];
	} else {
		data_ptr = NULL;
	};
};

template <class T>
__host__ __device__
Matrix<T>::Matrix(const Matrix &rhs) {
	n = rhs.n; m = rhs.m;
	if ( (rhs.n>0) && (rhs.m>0) ) {
		data_ptr = new T [n*m];
		for (int i=0; i<n*m; i++) data_ptr[i] = rhs.data_ptr[i];
	} else {
		data_ptr = NULL;
	};
};

template <class T>
__host__ __device__
Matrix<T> & Matrix<T>::operator=(const Matrix<T> &rhs) {
	// Postcondition: normal assignment via copying has been performed;
	// If matrix and rhs were different sizes, matrix has been resized to match the size of rhs
	if (this != &rhs) { // if we are not copying the same object
		if (n*m != rhs.n*rhs.m) {
			if (data_ptr != NULL) { // If this matrix is nonempty, release all mem
				delete[] (data_ptr);
			};
		};
		n = rhs.n; m = rhs.m;
		data_ptr = ((n>0)&&(m>0)) ? new T [n*m] : NULL;
		for (int i=0; i<n*m; i++) data_ptr[i] = rhs.data_ptr[i];
	};
	return *this;
};

template <class T>
__host__ __device__
inline T* Matrix<T>::operator[](const int i) { // pointer to column i
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr + i*n;
};

template <class T>
__host__ __device__
inline const T* Matrix<T>::operator[](const int i) const { // pointer to column i
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr + i*n;
};

template <class T>
__host__ __device__
inline T& Matrix<T>::operator()(const int i, const int j) { // accessing value at i,j
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n || j<0 || j>=m) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr[i+j*n];
};

template <class T>
__host__ __device__
inline const T& Matrix<T>::operator()(const int i, const int j) const { // accessing value at i,j
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n || j<0 || j>=m) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr[i+j*n];
};

template <class T>
__host__ __device__
inline T& Matrix<T>::operator()(const int i) { // accessing value at i,j
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n*m) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr[i];
};

template <class T>
__host__ __device__
inline const T& Matrix<T>::operator()(const int i) const { // accessing value at i,j
#ifdef _CHECKBOUNDS_
	if (i<0 || i >=n*m) {
		throw("Matrix subscript out of bounds.");
	};
#endif
	return data_ptr[i];
};

template <class T>
__host__ __device__
Matrix<T> Matrix<T>::operator*(const Matrix<T> &rhs) { // Multiplication of matrix
	// Perform C =  A (n-by-m) * B(n2-by-m2)
	int n2 = rhs.nrows(), m2 = rhs.ncols();
	if (m != n2) {throw("Matix sizes are not compatible.")};
	Matrix<T> C(n,m2,0.0);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j<m2; j++) {
			T temp=0.0;
			for (int l = 0; l < m; l++) {
					temp += data_ptr[i+l*n]*rhs.data_ptr[l+j*n2];
			};
			C(i,j) = temp;
		};
	};
	return C;
};

template <class T>
__host__ __device__
Matrix<T> Matrix<T>::operator-() { // Multiplication of matrix
	// Perform C =  A (n-by-m) * B(n2-by-m2)
	if (m == 0 || n == 0) {throw("Matix is empty.")};
	Matrix<T> C(n,m,0.0);
	T temp = 0.0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j<m; j++) {
			temp=data_ptr[i+j*n];
			C(i,j) = -temp;
		};
	};
	return C;
};

template <class T>
__host__ __device__
inline int Matrix<T>::nrows() const {
	return n;
};

template <class T>
__host__ __device__
inline int Matrix<T>::ncols() const {
	return m;
};

template <class T>
__host__ __device__
inline int Matrix<T>::size() const {
	return n*m;
};

template <class T>
__host__ __device__
void Matrix<T>::resize(int newn, int newm) {
	if (newn < 0 || newm < 0) {
		throw("New sizes of matrix can't be negative.");
	} else {
		if (newn != n || newm != m) {
			if (newn*newm != n*m) {
				throw("The new sizes are not congruent with old sizes.");
			} else {
				n = newn; m = newm;
			};
		};
	};
};

template <class T>
__host__ __device__
void Matrix<T>::assign(int newn, int newm, const T &a) {
	if (newn < 0 || newm < 0) {
		throw("New sizes of matrix can't be negative. Nothing is done.");
	} else {
		if (newn*newm != n*m) {
			if (data_ptr != NULL) {
				delete[] (data_ptr);
				data_ptr = new T [newn*newm];
			} else {
				data_ptr = new T [newn*newm];
			};
			// throw("The new sizes are not congruent with old sizes.");
		};
		n = newn; m = newm;
		for (int i=0; i<n*m; i++) data_ptr[i] = a;
	};
};

template <class T>
__host__ __device__
Matrix<T>::~Matrix() {
	if (data_ptr != NULL) delete[] (data_ptr);
};

// Some tyep defs
typedef Matrix<double> mat;

#endif // __CONTAINERS__
