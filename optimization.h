#include "containers.h"
#include "myLU.h"

// Linesearch algorithm
template <class T>
__host__ __device__
void lnsrch(const mat & xold, const double fold, const mat &g, mat &p, mat &x, double &f, const double stpmax, bool &check, T &func) {
	const double ALF=1.0e-4, TOLX=2.22045e-16;
	double a,alam,alam2=0.0,alamin,b,disc,f2=0.0;
	double rhs1,rhs2,slope=0.0,sum=0.0,temp,test,tmplam;
	int i,n=xold.nrows();
	check=false;
	for (i=0;i<n;i++) sum += p(i)*p(i);
	sum=sqrt(sum);
	if (sum > stpmax)
		for (i=0;i<n;i++)
			p(i) *= stpmax/sum;
	for (i=0;i<n;i++)
		slope += g(i)*p(i);
	if (slope >= 0.0) throw("Roundoff problem in lnsrch.");
	test=0.0;
	for (i=0;i<n;i++) {
		temp=abs(p(i))/max(abs(xold(i)),1.0);
		if (temp > test) test=temp;
	};
	alamin=TOLX/test;
	alam=1.0;
	for (;;) {
		for (i=0;i<n;i++) x(i)=xold(i)+alam*p(i);
		f=func(x);
		if (alam < alamin) {
			for (i=0;i<n;i++) x(i)=xold(i);
			check=true;
			return;
		} else if (f <= fold+ALF*alam*slope) return;
		else {
			if (alam == 1.0)
				tmplam = -slope/(2.0*(f-fold-slope));
			else {
				rhs1=f-fold-alam*slope;
				rhs2=f2-fold-alam2*slope;
				a=(rhs1/(alam*alam)-rhs2/(alam2*alam2))/(alam-alam2);
				b=(-alam2*rhs1/(alam*alam)+alam*rhs2/(alam2*alam2))/(alam-alam2);
				if (a == 0.0) tmplam = -slope/(2.0*b);
				else {
					disc=b*b-3.0*a*slope;
					if (disc < 0.0) tmplam=0.5*alam;
					else if (b <= 0.0) tmplam=(-b+sqrt(disc))/(3.0*a);
					else tmplam=-slope/(b+sqrt(disc));
				}
				if (tmplam>0.5*alam)
					tmplam=0.5*alam;
			};
		};
		alam2=alam;
		f2 = f;
		alam=max(tmplam,0.1*alam);
	};
};

template <class T>
struct NRfdjac {
	double EPS;
	T* func;
	__host__ __device__
	NRfdjac(T &funcc) {
		EPS = 1.0e-8;
		func = &funcc;
	};
	__host__ __device__
	mat operator() (const mat &x, const mat &fvec) {
		int n=x.nrows();
		mat df(n,n);
		mat xh=x;
		for (int j=0;j<n;j++) {
			double temp=xh(j);
			double h=EPS*abs(temp);
			if (h == 0.0) h=EPS;
			xh(j)=temp+h;
			h=xh(j)-temp;
			mat f = (*func)(xh);
			xh(j)=temp;
			for (int i=0;i<n;i++) {
				df(i,j) = (f(i)-fvec(i))/h;
			};
		};
		return df;
	}
};

template <class T>
struct NRfmin {
	mat fvec;
	T* func;
	int n;
	__host__ __device__
	NRfmin(T &funcc) {
		func = &funcc;
	};

	__host__ __device__
	double operator() (const mat &x) {
		n=x.nrows();
		double sum=0;
		fvec = (*func)(x);
		for (int i=0;i<n;i++) sum += (fvec(i)*fvec(i));
		return 0.5*sum;
	};
};

template <class T>
__host__ __device__
bool newt(mat &x,  T &vecfunc) {
	// check = false means ok, or maxiter exceeded
	// check = true  means suprious converges, though norm is small, the value function is not zero.
	bool check = false;
	const int MAXITS=200;
	const double TOLF=1.0e-8,TOLMIN=1.0e-12,STPMX=100.0;
	const double TOLX=2.22045e-16;
	int i,j,its,n=x.nrows();
	double den,f,fold,stpmax,sum,temp,test;
	mat g(n,1),p(n,1),xold(n,1);
	mat fjac(n,n,3.0);
	NRfmin<T> fmin(vecfunc);
	// NRfdjac<T> fdjac(vecfunc);
	mat &fvec=fmin.fvec;
	f=fmin(x);
	test=0.0;
	for (i=0;i<n;i++)
		if (abs(fvec(i)) > test) test=abs(fvec(i));
	if (test < 0.01*TOLF) {
		check=false;
		return check;
	};
	sum=0.0;
	for (i=0;i<n;i++) {
		sum += (x(i)*x(i));
	};
	stpmax=STPMX*max(sqrt(sum),double(n));
	for (its=0;its<MAXITS;its++) {
		// fjac=fdjac(x,fvec);
		fjac=vecfunc.jac(x);
		for (i=0;i<n;i++) {
			sum=0.0;
			for (j=0;j<n;j++) sum += fjac(j,i)*fvec(j);
			g(i)=sum;
		};
		for (i=0;i<n;i++) xold(i)=x(i);
		fold=f;
		for (i=0;i<n;i++) p(i) = -fvec(i);
		LUdcmp alu(fjac);
		alu.solve(p,p);
		lnsrch(xold,fold,g,p,x,f,stpmax,check,fmin);
		test=0.0;
		for (i=0;i<n;i++)
			if (abs(fvec(i)) > test) test=abs(fvec(i));
		if (test < TOLF) {
			check=false;
			return check;
		};
		if (check) {
			test=0.0;
			den=max(f,0.5*n);
			for (i=0;i<n;i++) {
				temp=abs(g(i))*max(abs(x(i)),1.0)/den;
				if (temp > test) test=temp;
			}
			check=(test < TOLMIN);
			return check;
		};
		test=0.0;
		for (i=0;i<n;i++) {
			temp=(abs(x(i)-xold(i)))/max(abs(x(i)),1.0);
			if (temp > test) test=temp;
		};
		if (test < TOLX)
			return false;
	};
	throw("MAXITS exceeded in newt");
	return false;
};

template<class T>
__host__ __device__
void newt_fast(mat& x, const T& func) {
	const int ntrial = 100;
	const double tolx = 1.0e-5;
	const double tolf = 1.0e-10;
	int i,n = x.nrows();
	mat p(n,1); 
	mat fvec(n,1);
	mat fjac(n,n);
	for (int k=0; k<ntrial; k++) {
		double errf = 0.0;
		fvec = func(x);
		fjac = func.jac(x);
		for (i=0;i<n;i++) errf += abs(fvec(i));
		if (errf <= tolf) return;
		for (i=0;i<n;i++) p(i) = -fvec(i);
		LUdcmp alu(fjac);
		alu.solve(p,p);
		double errx = 0.0;
		for (i=0;i<n;i++) {
			errx += abs(p(i));
			x(i) += p(i);
		};
		if (errx <= tolx) return;
	};
	return;
};

template<class T>
__host__ __device__
void newt_fast(double* x, T func,double* stack) {
	const int ntrial = 20;
	const double tolx = 1.0e-5;
	const double tolf = 1.0e-10;

	double* p = (stack + 8);
	double* fvec = (p + 8);
	double* fjac = (fvec + 8);
	double* indx = (fjac + 64);
	double* lu = (indx + 8);
	double* vvv = (lu + 64);
	int n = 8;

	for (int k=0; k<ntrial; k++) {
		double errf = 0.0;
		func(x,fvec);
		func.jac(x,fjac);
		for (int i=0;i<n;i++) errf += abs(fvec[i]);
		if (errf <= tolf) return;
		for (int i=0;i<n;i++) p[i] = -fvec[i];

		// Computes p = -inv(J)*F
		LUP(fjac,8,lu,indx,vvv);
		solve(lu,indx,p,8,1,p);

		double errx = 0.0;
		for (int i=0;i<n;i++) {
			errx += abs(p[i]);
			x[i] += p[i];
		};
		if (errx <= tolx) return;
	};
};
