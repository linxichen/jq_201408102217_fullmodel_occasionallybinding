clear all
state = [10,3,1,0.16];
%state = [9,4,1.2,0.2];

EM = [1.2,1.2,1.2]
u0 = [10 3 0.8 0.1 0.3 1.01 0.03 1];
tic
options = optimset('Display','iter-detailed','Jacobian','off');
u_nojacob = fsolve(@(u) binding(state,EM,u),u0,options);
toc

tic
options = optimset('Display','iter-detailed','Jacobian','on');
u_jacob = fsolve(@(u) binding(state,EM,u),u0,options);
toc

u_old = u0';
for i = 1:10
[F,J] = binding(state,EM,u_old);
dev = -J\F;
u_new = u_old + dev
u_old = u_new
end

u_jacob' - u_new
