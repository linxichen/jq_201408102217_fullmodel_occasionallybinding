function u = findcontrol(state,EM)
jq_para;
k = state(1);
b = state(2);
z = state(3);
xxi = state(4);
zkttheta = z*k^ttheta;

EMK = EM(1);
EMB = EM(2);
EMC = EM(3);

% Not Binding Case
mmu = 0;
R = EMK/EMB;
c = (1-ttau)/(R-ttau)/(bbeta*EMC);
llambda = c*bbeta*EMK;
d = dbar + (1/llambda-1)/(2*kkappa);
n0 = 0.3;
options = optimset('Display','none','Jacobian','off');
n = fsolve(@(n) notbindinghour(zkttheta,c,n),n0,options);
Y = zkttheta*n^(1-ttheta);
kplus = (1-ddelta)*k+Y-c-kkappa*(d-dbar)^2;
bplus = R*(aalpha*c*n/(1-n)+d+b-c);
u = [kplus bplus c d n R mmu llambda];

if (xxi*(kplus-bplus*(1-ttau)/(R-ttau)) > Y && c>0 && R>ttau && n>0 && n<1 && kplus>0)
    return
else
    u0 = [k b 0.8 0.1 0.3 1.01 0.03 1];
    u = fsolve(@(u) binding(state,EM,u),u0,options);
end

end