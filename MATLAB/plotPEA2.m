%% Housekeeping
clear all
close all
load 'PEA.mat';

%% Implied policy functions
%bss = 3.6368; zss = 1; xxiss = 
nk = 10; nb = 1; nz = 1; nxxi = 1;
Kgrid = linspace(kss*0.8,kss*1.20,nk);
Bgrid = 3.6368;%linspace(2,7,nb);
Zgrid = 1;% linspace(0.9,1.1,nz);
XXIgrid = 0.1634;% linspace(0.15,0.17,nxxi);
% kk = zeros(nk,nb,nz,nxxi);
% bb = zeros(nk,nb,nz,nxxi);
% cc = zeros(nk,nb,nz,nxxi);
% dd = zeros(nk,nb,nz,nxxi);
% nn = zeros(nk,nb,nz,nxxi);
% RR = zeros(nk,nb,nz,nxxi);
% mmummu = zeros(nk,nb,nz,nxxi);
% for i_k = 1:nk
%     for i_b = 1:nb
%         for i_z = 1:nz
%             for i_xxi = 1:nxxi
%                 state = [Kgrid(i_k) Bgrid(i_b) Zgrid(i_z) XXIgrid(i_xxi)];
%                 EM = exp([1 log(state)]*[coeff_mk coeff_mb coeff_mc]);
%                 u = findcontrol(state,EM);
% 
%                 kk(i_k,i_b,i_z,i_xxi) = real(u(1));%careful
%                 bb(i_k,i_b,i_z,i_xxi) = real(u(2));%careful
%                 cc(i_k,i_b,i_z,i_xxi) = real(u(3));
%                 dd(i_k,i_b,i_z,i_xxi) = real(u(3));
%                 nn(i_k,i_b,i_z,i_xxi) = real(u(3));
%                 RR(i_k,i_b,i_z,i_xxi) = real(u(6));%careful
%                 mmummu(i_k,i_b,i_z,i_xxi) = real(u(7));
%                 
%             end
%         end
%     end
% end









load 'alwaysbinding_results.mat';

order = oo_.dr.order_var';
ss = oo_.dr.ys';
ghx= oo_.dr.ghx';
ghu= oo_.dr.ghu';

Kgrid_bind=Kgrid;
Bgrid_bind=Bgrid;

for i=1:size(ghx,1)
    for j=1:size(order,2)
        index=find(order==j);
        newghx(i,j) = ghx(i,index);
    end
end
for i=1:size(ghu,1)
    for j=1:size(order,2)
        index=find(order==j);
        newghu(i,j) = ghu(i,index);
    end
end
endo_names= M_.endo_names;
for i=1:size(endo_names,1)
    if strcmp(endo_names(i,1),'k');
        break;
    end
end
jqklinear = ss(7) + newghx(2,7).*(log(Kgrid_bind)-ss(7));
jqklinear = exp(jqklinear);

jqclinear = ss(2) + newghx(2,2).*(log(Kgrid_bind)-ss(7));
jqclinear = exp(jqclinear);


jqdlinear = ss(6) + newghx(2,6).*(log(Kgrid_bind)-ss(7));
jqdlinear = exp(jqdlinear);

jqnlinear =ss(3) + newghx(2,3).*(log(Kgrid_bind)-ss(7));
jqnlinear = exp(jqnlinear);

jqmmulinear =ss(8) + newghx(2,8).*(log(Kgrid_bind)-ss(7));
jqmmulinear = exp(jqmmulinear);

jqblinear = ss(5) + newghx(2,5).*(log(Kgrid_bind)-ss(7));
jqblinear = exp(jqblinear);

jqRlinear = ss(4) + newghx(2,4).*(log(Kgrid_bind)-ss(7));
jqRlinear = exp(jqRlinear);























hFig = figure(1);
set(hFig, 'Position', [0 20 600 800])

linewitdh=1.5;
subplot(4,2,1)
 plot(Kgrid,squeeze(kk(:,1,1,1)),'LineWidth',linewitdh)
 hold on
  plot(Kgrid,jqklinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('k(t+1)')
legend('PEA','Linear','Location','SouthEast')


subplot(4,2,2)
 plot(Kgrid,squeeze(bb(:,1,1,1)),'LineWidth',linewitdh)
   hold on
  plot(Kgrid,jqblinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('b(t+1)')

subplot(4,2,3)
 plot(Kgrid,squeeze(cc(:,1,1,1)),'LineWidth',linewitdh)
   hold on
  plot(Kgrid,jqclinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('c(t)')

subplot(4,2,4)
 plot(Kgrid,squeeze(dd(:,1,1,1)),'LineWidth',linewitdh)
 hold on
  plot(Kgrid,jqdlinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('d(t)')

subplot(4,2,5)
 plot(Kgrid,squeeze(nn(:,1,1,1)),'LineWidth',linewitdh)
   hold on
  plot(Kgrid,jqnlinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('n(t)')

subplot(4,2,6)
 plot(Kgrid,squeeze(RR(:,1,1,1)),'LineWidth',linewitdh)
   hold on
  plot(Kgrid,jqRlinear,'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('R(t)')

subplot(4,2,7)
 plot(Kgrid,squeeze(mmummu(:,1,1,1)),'LineWidth',linewitdh)
   hold on
  plot(Kgrid(4:end),jqmmulinear(4:end),'r','LineWidth',linewitdh)
axis('tight')
xlabel('k(t)')
title('mmu(t)')


  set(gcf, 'PaperPositionMode', 'auto');

print -depsc2  'JQPEAvsLINEAR.eps' 
print -dpdf  'JQPEAvsLINEAR.pdf' 























