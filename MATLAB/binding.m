function [F,J] = binding(state,EM,u)
jq_para;
F = zeros(8,1); J = zeros(8,8);

% Loading states
k = state(1);
b = state(2);
z = state(3);
xxi=state(4);

% Loading expectations;
EMK = EM(1);
EMB = EM(2);
EMC = EM(3);

% Loading controls
kplus = u(1);
bplus = u(2);
c     = u(3);
d     = u(4);
n     = u(5);
R     = u(6);
mmu   = u(7);
llambda=u(8);

F(1) = llambda-1/(1+2*kkappa*(d-dbar));
J(1,8)=1; J(1,4)=2*kkappa*(1+2*kkappa*(d-dbar))^(-2);

F(2) = -bbeta*EMK+(llambda-mmu*xxi)/c;
J(2,3) = -(llambda-mmu*xxi)*c^(-2); 
J(2,8) = 1/c;
J(2,7) = -xxi/c;

F(3) = -bbeta*EMB+(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))/c;
J(3,3) = -(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))*c^(-2);
J(3,8) = 1/(c*R);
J(3,6) = (-llambda*R^(-2)+mmu*xxi*(1-ttau)*(R-ttau)^(-2))/c;
J(3,7) = -xxi*(1-ttau)/(c*(R-ttau));

F(4) = -bbeta*EMC+(1-ttau)/(R-ttau)/c;
J(4,3) = -c^(-2)*(1-ttau)/(R-ttau);
J(4,6) = -(1-ttau)*(R-ttau)^(-2)/c;

F(5) = aalpha*c/(1-n)-(1-mmu/llambda)*(1-ttheta)*z*k^(ttheta)*n^(-ttheta);
J(5,3) = aalpha/(1-n);
J(5,5) = aalpha*c*(1-n)^(-2)+(1-mmu/llambda)*(1-ttheta)*z*k^(ttheta)*ttheta*n^(-ttheta-1);
J(5,7) = (1-ttheta)*z*k^(ttheta)*n^(-ttheta)/llambda;
J(5,8) = -llambda^(-2)*mmu*(1-ttheta)*z*k^(ttheta)*n^(-ttheta);

F(6) = (1-ddelta)*k+z*k^(ttheta)*n^(1-ttheta)-c-kplus-kkappa*(d-dbar)^2;
J(6,5) = z*k^(ttheta)*(1-ttheta)*n^(-ttheta);
J(6,3) = -1;
J(6,1) = -1;
J(6,4) = -2*kkappa*(d-dbar);

F(7) = aalpha*c*n/(1-n)+b+d-c-bplus/R;
J(7,3) = aalpha*n/(1-n)-1;
J(7,5) = aalpha*c/(1-n)+aalpha*c*n*(1-n)^(-2);
J(7,4) = 1;
J(7,2) = -1/R;
J(7,6) = bplus*R^(-2);

F(8) = xxi*kplus-xxi*bplus*(1-ttau)/(R-ttau)-z*k^(ttheta)*n^(1-ttheta);
J(8,1) = xxi;
J(8,2) = -xxi*(1-ttau)/(R-ttau);
J(8,6) = xxi*bplus*(1-ttau)*(R-ttau)^(-2);
J(8,5) = -z*k^(ttheta)*(1-ttheta)*n^(-ttheta);

end