# ifndef __mylu__
# define __mylu__

__device__ __host__
double LUP(double* A, int n, double* lu, double* indx, double* vv) {
	// Initialization
	for (int i=0; i<n*n; i++) {
		lu[i] = A[i];
	};
	for (int i=0; i<n; i++) {
		indx[i] = 0.0;
	};

	// Don't really know what's going on
	const double TINY=1.0e-40;
	int i,imax,j;
	double big,temp;
	for (int i=0; i<n; i++) {
		vv[i] = 0.0;
	};

	double d = 1.0;
	for (int i=0;i<n;i++) {
		big=0.0;
		for (int j=0;j<n;j++)
			if ( (temp=abs(lu[i+j*n]))> big) big=temp;
		if (big == 0.0)	throw("Singular matrix in LUdcmp");
		vv[i]=1.0/big;
	};

	for (int k=0;k<n;k++) {
		big=0.0;
		for (i=k;i<n;i++) {
			temp=vv[i]*abs(lu[i+k*n]);
			if (temp > big) {
				big=temp;
				imax=i;
			};
		};
		if (k != imax) {
			for (j=0;j<n;j++) {
				temp=lu[imax+j*n];
				lu[imax+j*n]=lu[k+j*n];
				lu[k+j*n]=temp;
			}
			d = -d;
			vv[imax]=vv[k];
		};
		indx[k]=double(imax);
		if (lu[k+k*n] == 0.0) lu[k+k*n]=TINY;
		for (i=k+1;i<n;i++) {
			temp=lu[i+k*n] /= lu[k+k*n];
			for (j=k+1;j<n;j++)
				lu[i+j*n] -= temp*lu[k+j*n];
		};
	};
	
	return d;
};

__device__ __host__
void solve(double* lu, double* indx, double* b, int n, int m, double* x)
{
	// b is a mtrix of n-by-m
	for (int i_c=0; i_c<m; i_c++) {
		int i,ii=0,ip,j;
		double sum;
		for (i=0;i<n;i++) x[i+i_c*n] = b[i+i_c*n];
		for (i=0;i<n;i++) {
			ip=indx[i];
			sum=x[ip+i_c*n];
			x[ip+i_c*n]=x[i+i_c*n];
			if (ii != 0)
				for (j=ii-1;j<i;j++) sum -= lu[i+j*n]*x[j+i_c*n];
			else if (sum != 0.0)
				ii=i+1;
			x[i+i_c*n]=sum;
		};
		for (i=n-1;i>=0;i--) {
			sum=x[i+i_c*n];
			for (j=i+1;j<n;j++) sum -= lu[i+j*n]*x[j+i_c*n];
			x[i+i_c*n]=sum/lu[i+i*n];
		};
	};
};

#endif
