function [G,mk,mb,mc,R,kplus,bplus] = survival_cdn(k,b,z,xxi,c,d,n)
    dbar = 0.1147858823737546;
    kkappa = 0.1460;
    ttheta = 0.36;
    aalpha = 1.8834;
    ddelta = 0.025;
    ttau = 0.35;
    
    Y = z*k^ttheta*n^(1-ttheta);
    llambda = 1/(1+2*kkappa*(d-dbar));
    MPN = (1-ttheta)*Y/n;
    mmu = llambda*(1-aalpha*c/(1-n)/MPN);
    kplus = (1-ddelta)*k+Y-c-kkappa*(d-dbar)^2;
    boverR = aalpha*c*n/(1-n)+b+d-c;
    G = (kplus-Y/xxi)/boverR;
    % disp(abs(G+ttau-1));
    R = G*ttau/(G+ttau-1);
    bplus = boverR*R;
    
    mk = 1/c*(llambda*(1-ddelta)+(llambda-mmu)*ttheta*z*(k/n)^(ttheta-1));
    mb = 1/c*llambda;
    mc = 1/c;
   
end