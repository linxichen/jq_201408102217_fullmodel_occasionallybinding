clear all
clc 
close all

nk = 70;
nb = 70;
nz = 70;
nxxi = 70;

mmummu = zeros(nk,nb,nz,nxxi);
RR = mmummu;
kpluskplus = mmummu;
bplusbplus = mmummu;
mk = RR;
mb = RR;
mc = RR;
GG = RR;
bplusbplus_lin = mmummu;
RR_lin = mmummu;

KK = linspace(9,11,nk);
BB = linspace(3,4,nb);
ZZ = linspace(0.95,1.05,nz);
XXIXXI = linspace(0.16,0.17,nxxi);

for i_k= 1:nk
    for i_b = 1:nb
        for i_z = 1:nz
            for i_xxi = 1:nxxi
            k = KK(i_k);
            b = BB(i_b);
            z = ZZ(i_z);
            xxi = XXIXXI(i_xxi);
            
            c = exp(-0.209273...
                +0.007981*(log(b)-1.29111)...
                +0.640784*(log(k)-2.31052)...
                +0.001435*(log(z)-log(1))/0.0045...
                -0.000466*(log(xxi)+1.81155)/0.0098...
                );
            
            d = exp(-2.164687...
                -7.921001*(log(b)-1.29111)...
                +17.976911*(log(k)-2.31052)...
                -0.059031*(log(z)-log(1))/0.0045...
                +0.119313*(log(xxi)+1.81155)/0.0098...
                );            
                
            n = exp(-1.203965...
                -0.624056*(log(b)-1.29111)...
                +1.232885*(log(k)-2.31052)...
                -0.004255*(log(z)-log(1))/0.0045...
                +0.011592*(log(xxi)+1.81155)/0.0098...
                );
        
            RR_lin(i_k,i_b,i_z,i_xxi) ...
                = exp(0.011511...
                -0.020137*(log(b)-1.29111)...
                +0.023350*(log(k)-2.31052)...
                -0.000041*(log(z)-log(1))/0.0045...
                +0.000372*(log(xxi)+1.81155)/0.0098...
                );
            bplusbplus_lin(i_k,i_b,i_z,i_xxi) ...
                = exp(1.291113...
                +0.575832*(log(b)-1.29111)...
                +0.890194*(log(k)-2.31052)...
                -0.003096*(log(z)-log(1))/0.0045...
                +0.007217*(log(xxi)+1.81155)/0.0098...
                );
            
            [GG(i_k,i_b,i_z,i_xxi),mk(i_k,i_b,i_z,i_xxi),mb(i_k,i_b,i_z,i_xxi),mc(i_k,i_b,i_z,i_xxi),RR(i_k,i_b,i_z,i_xxi),kpluskplus(i_k,i_b,i_z,i_xxi),bplusbplus(i_k,i_b,i_z,i_xxi)]...
            = survival_cdn(k,b,z,xxi,c,d,n);
  
            end
        end
    end
end

%%
max(abs(bplusbplus(:)-bplusbplus_lin(:)))
max(abs(RR(:)-RR_lin(:)))

%%
z_index = floor(nz/2);
xxi_index = floor(nxxi/2);
% figure
% surf(BB,KK,squeeze(kpluskplus(:,:,z_index,xxi_index)-kpluskplus_lin(:,:,z_index,xxi_index)));
% xlabel('debt')
% ylabel('capital')
% title('capital tomorrow discrepancy')

figure
surf(BB,KK,squeeze(RR(:,:,z_index,xxi_index)-RR_lin(:,:,z_index,xxi_index)));
xlabel('debt')
ylabel('capital')
title('interest rate discrepancy')

figure
surf(BB,KK,squeeze(RR(:,:,z_index,xxi_index)),40*ones(nk,nz));
hold on
surf(BB,KK,squeeze(RR_lin(:,:,z_index,xxi_index)),30*ones(nk,nz));
xlabel('debt')
ylabel('capital')
title('interest rate comparison')
zlim([1.006 1.02])
legend('Nonlinear','Linear')
hold off
%%

figure
surf(NN,CC,squeeze(bplusbplus(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('debt tomorrow')

figure
surf(NN,CC,squeeze(RR(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('Interest Rate')

figure
surf(NN,CC,squeeze(mk(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mk')

figure
surf(NN,CC,squeeze(mb(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mb')

figure
surf(NN,CC,squeeze(mc(:,d_index,:)));
xlabel('hours')
ylabel('consumption')
title('mc')
%%
figure
surf(DD,CC,squeeze(kpluskplus(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('capital tomorrow')

figure
surf(DD,CC,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('debt tomorrow')

figure
surf(DD,CC,squeeze(RR(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('Interest Rate')

figure
surf(DD,CC,squeeze(mk(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('mk')

figure
surf(DD,CC,squeeze(mb(:,:,end)));
ylabel('dividend')
xlabel('consumption')
title('mb')

figure
surf(DD,CC,squeeze(mc(:,:,end)));
ylabel('dividend')
xlabel('hours')
title('mc')

figure
surf(DD,CC,squeeze(bplusbplus(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('debt tomorrow')

figure
surf(DD,CC,squeeze(RR(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('Interest')

%% 
list = RR>0.35;
RR_filter = list.*RR;
figure
surf(DD,CC,squeeze(RR_filter(:,:,end)));
ylabel('dividend')
xlabel('hour')
title('Interest Survived')