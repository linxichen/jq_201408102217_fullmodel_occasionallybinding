clear all
clc 
close all

nmk = 70;
nmb = 70;
nmc = 70;


mmummu = zeros(nmk,nmb,nmc);
RR = zeros(nmk,nmb,nmc);
cc = RR;
dd = RR;
nn = RR;
kpluskplus = RR;
bplusbplus = RR;
BRBR = RR;
A = RR;

MKOVERMB = linspace(0.9751,1.07,nmk);
MBOVERMC = linspace(0.626,0.7,nmb);
MC = linspace(3.15,3.2,nmc);

for i_mk = 1:nmk
    for i_mb = 1:nmb
        for i_mc = 1:nmc
            mkovermb = MKOVERMB(i_mk);
            mbovermc = MBOVERMC(i_mb);
            mc = MC(i_mc);
            [A(i_mk,i_mb,i_mc),BRBR(i_mk,i_mb,i_mc),mmummu(i_mk,i_mb,i_mc),RR(i_mk,i_mb,i_mc),cc(i_mk,i_mb,i_mc),dd(i_mk,i_mb,i_mc),nn(i_mk,i_mb,i_mc),kpluskplus(i_mk,i_mb,i_mc),bplusbplus(i_mk,i_mb,i_mc)]...
            = survival(10.0797,3.6368,1.0,0.1634,mkovermb,mbovermc,mc);
        end
    end
end

%%
kill = (abs(bplusbplus)<5);
bpbp = bplusbplus.*kill;
surf(MC,MKOVERMB,squeeze(RR(:,20,:)));
xlabel('mkovermb')
ylabel('mc')
%%
k = 10.07971431738234;
b = 3.636832767283912;
z = 1;
xxi = 0.1634;
mk = 1.247007176273398+0.00;
mb = 1.232781873263119;
mc = 1.232781873263119;
[A,boverR,mmu,R,c,d,n,kplus,bplus] = survival_raw(k,b,z,xxi,mk,mb,mc);