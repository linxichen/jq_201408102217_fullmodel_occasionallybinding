#include <iostream>
#include <iomanip>
#include <fstream>
#include "containers.h"

// Define an class that contains parameters and steady states
struct para {
	// Model parameters
	double aalpha;
	double bbeta ;
	double ddelta;
	double ttheta;
	double kkappa;
	double ttau  ;
	double xxibar;
	double zbar  ;
	double dbar  ;
	double rrhozz;
	double rrhozxxi;
	double rrhoxxiz;
	double rrhoxxixxi;
	double var_epsz;
	double var_epsxxi;
	double A[4];
	double Ssigma_e[4];

	// Steady States
	double kss;
	double bss;
	double nss;
	double css;
	double wss;
	double dss;
	double mmuss;
	double mkss;
	double mbss;
	double mcss;
	double yss;
	double Rss;
	double invss;


	// Find steady state and find aalpha based steady state target
	__host__ __device__
	void complete() {
		// Fill A and Ssigma_e
		A[0] = rrhozz; A[2] = rrhozxxi;
		A[1] = rrhoxxiz; A[3] = rrhoxxixxi;
		Ssigma_e[0] = var_epsz;
		Ssigma_e[1] = 0;
		Ssigma_e[2] = 0;
		Ssigma_e[3] = var_epsxxi;

		// Find aalpha based on SS computation
		Rss = (1/bbeta)*(1-ttau) + ttau;
		mmuss = (1-Rss*bbeta)/xxibar*(Rss-ttau)/(Rss*(1-ttau));
		double nsskss = pow( ( (1-xxibar*mmuss)/bbeta - 1 + ddelta )/(ttheta*zbar*(1-mmuss)),1/(1-ttheta) ); 
		double kssnss = 1/nsskss;
		wss = (1-ttheta)*zbar*pow(kssnss,ttheta)*(1-mmuss);
		double bssnss = (kssnss - zbar/xxibar*pow(kssnss,ttheta))*(Rss-ttau)/(1-ttau);
		double dssnss = (1-ddelta)*kssnss + zbar*pow(kssnss,ttheta) - wss -bssnss*(1-1/Rss)-kssnss;
		double aux = (wss + bssnss*(1-1/Rss)+dssnss)*aalpha/wss;
		nss = 1/(1+aux);
		kss = kssnss*nss;
		bss = bssnss*nss;
		dss = dssnss*nss;
		css = wss*(1-nss)/aalpha;
		yss = zbar*pow(kss,ttheta)*pow(nss,1-ttheta);
		invss = kss - (1-ddelta)*kss;
		mcss = 1/css;
		mbss = 1/css;
		mkss = 1/css*(1-ddelta+(1-mmuss)*ttheta*zbar*pow(kssnss,ttheta-1));
	};

	// Export parameters to a .m file in MATLAB syntax
	__host__
	void exportmatlab(std::string filename) {
		std::ofstream fileout(filename.c_str(), std::ofstream::trunc);

		// Model Parameters
		fileout << std::setprecision(16) << "aalpha=" << aalpha << ";"<< std::endl;
		fileout << std::setprecision(16) << "bbeta=" << bbeta << ";"<< std::endl;
		fileout << std::setprecision(16) << "ddelta=" << ddelta << ";"<< std::endl;
		fileout << std::setprecision(16) << "ttheta=" << ttheta << ";"<< std::endl;
		fileout << std::setprecision(16) << "ttau=" << ttau << ";"<< std::endl;
		fileout << std::setprecision(16) << "kkappa=" << kkappa << ";"<< std::endl;
		fileout << std::setprecision(16) << "xxibar=" << xxibar << ";"<< std::endl;
		fileout << std::setprecision(16) << "zbar=" << zbar << ";"<< std::endl;
		fileout << std::setprecision(16) << "rrhozz=" << rrhozz << ";"<< std::endl;
		fileout << std::setprecision(16) << "rrhozxxi=" << rrhozxxi << ";"<< std::endl;
		fileout << std::setprecision(16) << "rrhoxxiz=" << rrhoxxiz << ";"<< std::endl;
		fileout << std::setprecision(16) << "rrhoxxixxi=" << rrhoxxixxi << ";"<< std::endl;
		fileout << std::setprecision(16) << "ssigmaepsz=" << sqrt(var_epsz) << ";"<< std::endl;
		fileout << std::setprecision(16) << "ssigmaepsxxi=" << sqrt(var_epsxxi) << ";"<< std::endl;

		// Steady States
		fileout << std::setprecision(16) << "kss=" << kss << ";"<< std::endl;
		fileout << std::setprecision(16) << "bss=" << bss << ";"<< std::endl;
		fileout << std::setprecision(16) << "nss=" << nss << ";"<< std::endl;
		fileout << std::setprecision(16) << "css=" << css << ";"<< std::endl;
		fileout << std::setprecision(16) << "wss=" << wss << ";"<< std::endl;
		fileout << std::setprecision(16) << "dss=" << dss << ";"<< std::endl;
		fileout << std::setprecision(16) << "mmuss=" << mmuss << ";"<< std::endl;
		fileout << std::setprecision(16) << "mkss=" << mkss << ";"<< std::endl;
		fileout << std::setprecision(16) << "yss=" << yss << ";"<< std::endl;
		fileout << std::setprecision(16) << "Rss=" << Rss << ";"<< std::endl;
		fileout << std::setprecision(16) << "invss=" << invss << ";"<< std::endl;
		fileout.close();
	};
};

void linearizedmodel(double* A, double* B, double* C, double* rrho, int n, int n_shock, para p) {
	// HH Budget. Correct.
	B[0+3*n] = p.nss;
	B[0+2*n] = p.wss;
	B[0+4*n] = 1;
	B[0+1*n] = -1;

	// Labor Demand. Correct
	B[1+5*n] = (p.ttheta-1)*p.yss/p.nss;
	B[1+6*n] = (1-p.ttheta)*(1-p.mmuss)/p.nss;
	B[1+2*n] = -(1-p.ttheta)*(1-p.mmuss)*p.yss/(p.nss*p.nss);
	B[1+3*n] = -1;

	// Labor Supply. Correct
	B[2+1*n] = p.aalpha/(1-p.nss);
	B[2+2*n] = p.aalpha*p.css/((1-p.nss)*(1-p.nss));
	B[2+3*n] = -1;

	// Capital Demand. Correct.
	A[3+8*n] = p.bbeta; 
	B[3+1*n] = -(1-p.mmuss*p.xxibar)/(p.css*p.css); 
	B[3+5*n] = -p.xxibar/p.css; 
	C[3+1*n] = -p.mmuss*p.xxibar/p.css;

	// Resource Constraint. Correct
	A[4+0*n] = 1; 
	B[4+0*n] = 1-p.ddelta; 
	B[4+6*n] = 1; 
	B[4+1*n] = -1;

	// Financial Constraint. Fixed.
	A[5+0*n] = p.xxibar;
	B[5+6*n] = 1;
	C[5+1*n] = -p.xxibar*p.kss;

	// Output Definition. Correct
	C[6+0*n] = p.yss;
	B[6+0*n] = p.ttheta*p.yss/p.kss;
	B[6+2*n] = (1-p.ttheta)*p.yss/p.nss;
	B[6+6*n] = -1;

	// Investment Definition. Correct
	A[7+0*n] = 1;
	B[7+7*n] = 1;
	B[7+0*n] = 1-p.ddelta;

	// MK defintion:
	B[8+1*n] = -pow(p.css,-2)*(1-p.ddelta+(1-p.mmuss)*p.ttheta*p.yss/p.kss); 
	B[8+5*n] = -p.ttheta*p.yss/(p.css*p.kss); 
	B[8+6*n] = (1-p.mmuss)*p.ttheta/(p.css*p.kss); 
	B[8+0*n] = -(1-p.mmuss)*p.ttheta*p.yss*pow(p.kss,-2)/p.css;
	B[8+8*n] = -1;

	for (int i=0; i< n_shock*n_shock; i++) {
		rrho[i] = p.A[i];
	};
};

// Define state struct that contains "natural" state 
struct state {
	// Data member
	double k, b, z, xxi, zkttheta;

	// Constructor
	__host__ __device__
	state(double _k, double _b, double _z, double _xxi, para& p) {
		k = _k;
		b = _b;
		z = _z;
		xxi = _xxi;
		zkttheta = _z*pow(_k,p.ttheta);
	};

	// Alternative constructor
	__host__ __device__
	state(double _k, double _b, double _z, double _xxi, double _zkttheta) {
		k = _k;
		b = _b;
		z = _z;
		xxi = _xxi;
		zkttheta = _zkttheta;
	};
};

// Define shadow struct that contains all shadow values
struct shadow {
	// Data member
	double mk, mb, mc, mkovermb, mbovermc;

	// Constructor
	__host__ __device__
	shadow(double _mk, double _mb, double _mc) {
		mk = _mk;
		mb = _mb;
		mc = _mc;
		mkovermb = mk/mb;
		mbovermc = mb/mc;
	};

	// Constructor
	__host__ __device__
	shadow(double _mkovermb, double _mbovermc, double _mc, int flag) {
		mkovermb = _mkovermb;
		mbovermc = _mbovermc;
		mc = _mc;
		mb = mc*mbovermc;
		mk = mb*mkovermb;
	};
};

// Define policy guesses.
struct guess {
	// Data member
	double d, n, R;

	// Constructor
	__host__ __device__
	guess(double _d, double _n, double _R) {
		d = _d;
		n = _n;
		R = _R;
	};
};

struct expectation {
	// Data Member
	double EMK,EMB,EMC;

	// Constructor
	__host__ __device__
	expectation(double _EMK, double _EMB, double _EMC) {
		EMK = _EMK;
		EMB = _EMB;
		EMC = _EMC;
	};
};

// Function to be solved in order to find the implied n
struct notbindinghour {
	// Data members, constants and coefficients
	double czero, coneminusttheta, cminusttheta;
	double ttheta;

	// Constructor
	__host__ __device__
	notbindinghour(state s, double c, para p) {
		czero = -p.aalpha*c;
		coneminusttheta = -(1-p.ttheta)*s.zkttheta;
		cminusttheta = (1-p.ttheta)*s.zkttheta;
		ttheta = p.ttheta;
	};

	// Value operator
	__host__ __device__
	double operator()(double n) {
		return czero+coneminusttheta*pow(n,1-ttheta)+cminusttheta*pow(n,-ttheta);
	};

	// Derivative
	__host__ __device__
	double prime(double n) {
		return (1-ttheta)*coneminusttheta*pow(n,-ttheta)+(-ttheta)*cminusttheta*pow(n,-ttheta-1);
	};
};

// Functor to find control variables when binding (PEA)
struct bindingcontrols {
	// Data members
	double k,b,z,xxi;
	double EMK,EMB,EMC;
	double dbar,bbeta,ttau,ttheta,kkappa,aalpha, ddelta;

	// Constructor
	__host__ __device__
	bindingcontrols(state& s, expectation& e, para & p) {
		k = s.k; b = s.b; z = s.z; xxi = s.xxi;
		EMK = e.EMK; EMB = e.EMB; EMC = e.EMC;
		dbar = p.dss; bbeta = p.bbeta; ttau = p.ttau; ttheta = p.ttheta; kkappa = p.kkappa; aalpha = p.aalpha; ddelta = p.ddelta;
	};

	// Get value 
	__host__ __device__
	mat operator()(mat & x) {
		if ((x.ncols() != 1) || (x.nrows() <= 0)) {
			throw("input arugment is not a vector!.\n")
		};
		double kplus = x(0);
	   	double bplus = x(1); 
		double c = x(2);
	  	double d = x(3);
	   	double n = x(4);
	   	double R = x(5);
	   	double mmu = x(6);
	   	double llambda = x(7);
		
		mat F(x.nrows(),1,0.0);
		double Y = z*pow(k/n,ttheta)*n;
		F(0) = llambda-1/(1+2*kkappa*(d-dbar));
		F(1) = -bbeta*EMK+(llambda-mmu*xxi)/c;
		F(2) = -bbeta*EMB+(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))/c;
		F(3) = -bbeta*EMC+(1-ttau)/(R-ttau)/c;
		F(4) = aalpha*c/(1-n)-(1-mmu/llambda)*(1-ttheta)*Y/n;
		F(5) = (1-ddelta)*k+Y-c-kplus-kkappa*(d-dbar)*(d-dbar);
		F(6) = aalpha*c*n/(1-n)+b+d-c-bplus/R;
		F(7) = xxi*kplus-xxi*bplus*(1-ttau)/(R-ttau)-Y;
		return F;
	};

	__host__ __device__
	void operator()(double* x, double* F) {
		double kplus = x[0];
	   	double bplus = x[1]; 
		double c = x[2];
	  	double d = x[3];
	   	double n = x[4];
	   	double R = x[5];
	   	double mmu = x[6];
	   	double llambda = x[7];
		
		double Y = z*pow(k/n,ttheta)*n;
		F[0] = llambda-1/(1+2*kkappa*(d-dbar));
		F[1] = -bbeta*EMK+(llambda-mmu*xxi)/c;
		F[2] = -bbeta*EMB+(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))/c;
		F[3] = -bbeta*EMC+(1-ttau)/(R-ttau)/c;
		F[4] = aalpha*c/(1-n)-(1-mmu/llambda)*(1-ttheta)*Y/n;
		F[5] = (1-ddelta)*k+Y-c-kplus-kkappa*(d-dbar)*(d-dbar);
		F[6] = aalpha*c*n/(1-n)+b+d-c-bplus/R;
		F[7] = xxi*kplus-xxi*bplus*(1-ttau)/(R-ttau)-Y;
	};

	// Get Jacobian 
	__host__ __device__
	mat jac(mat & x) {
		if ((x.ncols() != 1) || (x.nrows() <= 0)) {
			throw("input arugment is not a vector!.\n")
		};
		double kplus = x(0);
	   	double bplus = x(1); 
		double c = x(2);
	  	double d = x(3);
	   	double n = x(4);
	   	double R = x(5);
	   	double mmu = x(6);
	   	double llambda = x(7);
		
		mat J(x.nrows(),x.nrows(),0.0);
		double Y = z*pow(k/n,ttheta)*n;

		// Row-by-row input
		J(0,7)=1; J(0,3)=2*kkappa*pow(1+2*kkappa*(d-dbar),-2.0);

		J(1,2) = -(llambda-mmu*xxi)/(c*c); 
		J(1,7) = 1/c;
		J(1,6) = -xxi/c;

		J(2,2) = -(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))/(c*c);
		J(2,7) = 1/(c*R);
		J(2,5) = (-llambda/(R*R)+mmu*xxi*(1-ttau)*pow(R-ttau,-2.0))/c;
		J(2,6) = -xxi*(1-ttau)/(c*(R-ttau));

		J(3,2) = -pow(c,-2.0)*(1-ttau)/(R-ttau);
		J(3,5) = -(1-ttau)*pow(R-ttau,-2.0)/c;

		J(4,2) = aalpha/(1-n);
		J(4,4) = aalpha*c*pow(1-n,-2)+(1-mmu/llambda)*(1-ttheta)*ttheta*Y/(n*n);
		J(4,6) = (1-ttheta)*Y/n/llambda;
		J(4,7) = -pow(llambda,-2)*mmu*(1-ttheta)*Y/n;

		J(5,4) = (1-ttheta)*Y/n;
		J(5,2) = -1;
		J(5,0) = -1;
		J(5,3) = -2*kkappa*(d-dbar);

		J(6,2) = aalpha*n/(1-n)-1;
		J(6,4) = aalpha*c/(1-n)+aalpha*c*n*pow(1-n,-2);
		J(6,3) = 1;
		J(6,1) = -1/R;
		J(6,5) = bplus/(R*R);

		J(7,0) = xxi;
		J(7,1) = -xxi*(1-ttau)/(R-ttau);
		J(7,5) = xxi*bplus*(1-ttau)*pow(R-ttau,-2);
		J(7,4) = -(1-ttheta)*Y/n;

		return J;
	};

	// Get Jacobian 
	__host__ __device__
	void jac(double* x, double* J) {
		// double kplus = x[0];
	   	double bplus = x[1]; 
		double c =     x[2];
	  	double d =     x[3];
	   	double n =     x[4];
	   	double R =     x[5];
	   	double mmu =   x[6];
	   	double llambda=x[7];
		
		double Y = z*pow(k/n,ttheta)*n;

		// Row-by-row input
		J[0+7*8]=1; 
		J[0+3*8]=2*kkappa*pow(1+2*kkappa*(d-dbar),-2.0);

		J[1+2*8] = -(llambda-mmu*xxi)/(c*c); 
		J[1+7*8] = 1/c;
		J[1+6*8] = -xxi/c;

		J[2+2*8] = -(llambda/R-mmu*xxi*(1-ttau)/(R-ttau))/(c*c);
		J[2+7*8] = 1/(c*R);
		J[2+5*8] = (-llambda/(R*R)+mmu*xxi*(1-ttau)*pow(R-ttau,-2.0))/c;
		J[2+6*8] = -xxi*(1-ttau)/(c*(R-ttau));

		J[3+2*8] = -pow(c,-2.0)*(1-ttau)/(R-ttau);
		J[3+5*8] = -(1-ttau)*pow(R-ttau,-2.0)/c;

		J[4+2*8] = aalpha/(1-n);
		J[4+4*8] = aalpha*c*pow(1-n,-2)+(1-mmu/llambda)*(1-ttheta)*ttheta*Y/(n*n);
		J[4+6*8] = (1-ttheta)*Y/n/llambda;
		J[4+7*8] = -pow(llambda,-2)*mmu*(1-ttheta)*Y/n;

		J[5+4*8] = (1-ttheta)*Y/n;
		J[5+2*8] = -1;
		J[5+0*8] = -1;
		J[5+3*8] = -2*kkappa*(d-dbar);

		J[6+2*8] = aalpha*n/(1-n)-1;
		J[6+4*8] = aalpha*c/(1-n)+aalpha*c*n*pow(1-n,-2);
		J[6+3*8] = 1;
		J[6+1*8] = -1/R;
		J[6+5*8] = bplus/(R*R);

		J[7+0*8] = xxi;
		J[7+1*8] = -xxi*(1-ttau)/(R-ttau);
		J[7+5*8] = xxi*bplus*(1-ttau)*pow(R-ttau,-2);
		J[7+4*8] = -(1-ttheta)*Y/n;

	};
};
// struct of variables implied by natural state, optionally shadow value/kplus tomorrow
struct control {
	// Data member
	double kplus, bplus, c, n, w, d, mmu, Y, lhsk, lhsb, lhsc, llambda, R;

	// finding the control variables (adrian's method and projection)
	__device__
	void compute(state s, shadow m, para p, int binding) {
		if (binding == 1) {
			// Case 1: Binding
			c = 1/m.mc;
			llambda = m.mb/m.mc;
			d = p.dss + (1/llambda-1)/(2*p.kkappa);
			double G = (1-p.ttheta)*s.k*(m.mk*c/llambda-1+p.ddelta)/(p.ttheta*p.aalpha*c);
			n = G/(1+G);
			Y = s.z*pow(s.k/n,p.ttheta)*n;
			mmu = llambda*( 1-p.aalpha*c*G/((1-p.ttheta)*Y) );
			w = p.aalpha*c/(1-n);
			double boverR = w*n + s.b + d - c;
			kplus = (1-p.ddelta)*s.k + Y - c - p.kkappa*(d-p.dss)*(d-p.dss);
			double A = (kplus - Y/s.xxi)/boverR;
			R = p.ttau*A/(p.ttau+A-1);
			bplus = boverR*R;
			lhsk = (llambda-mmu*s.xxi)/c;
			lhsb = (llambda/R-mmu*s.xxi*(1-p.ttau)/(R-p.ttau))/c;
			lhsc = ((1-p.ttau)/(R-p.ttau))/c;
		};
	};

	// Overloaded compute
	__host__ __device__
	void compute(state s, guess g, para p) {
		d = g.d;
		n = g.n;
		R = g.R;
		Y = s.z*pow(s.k/n,p.ttheta)*n;
		llambda = 1/(1+2*p.kkappa*(d-p.dss));
		double nomin = s.b+d+(p.aalpha*n/(1-n)-1)*( (1-p.ddelta)*s.k+Y*(1-1/s.xxi)-p.kkappa*(d-p.dss)*(d-p.dss)  );
		double denom = 1/R - (1-p.aalpha*n/(1-n))*(1-p.ttau)/(R-p.ttau);
		bplus = nomin/denom;
		kplus = Y/s.xxi + bplus*(1-p.ttau)/(R-p.ttau);
		c = (1-p.ddelta)*s.k + Y - p.kkappa*(d-p.dss)*(d-p.dss) - kplus;
		w = p.aalpha*c/(1-n);
		double MPN = (1-p.ttheta)*s.z*pow(s.k/n,p.ttheta);
		mmu = llambda*(1-w/MPN);
		lhsk = (llambda-mmu*s.xxi)/c;
		lhsb = (llambda/R-mmu*s.xxi*(1-p.ttau)/(R-p.ttau))/c;
		lhsc = ((1-p.ttau)/(R-p.ttau))/c;
	};

	// Overloaded compute for PEA style algorithm, occasionally
	__device__ __host__
	void compute(state s, expectation e, para p, double* stack) {
		// Case 0: the not binding case first
		mmu = 0;
		R = e.EMK/e.EMB;
		c = (1-p.ttau)/(R-p.ttau)/(p.bbeta*e.EMC);
		llambda = c*p.bbeta*e.EMK;
		d = p.dss + (1/llambda-1)/(2*p.kkappa);
		double n0 = p.nss;
		n = newton(notbindinghour(s,c,p),0.0,1.0,n0);
		Y = s.zkttheta*pow(n,1-p.ttheta);
		kplus = (1.0-p.ddelta)*s.k + Y - c - p.kkappa*(d-p.dss)*(d-p.dss);
		bplus = R*(p.aalpha*c*n/(1-n)+d+s.b-c);
		lhsk = (llambda-mmu*s.xxi)/c;
		lhsb = (llambda/R-mmu*s.xxi*(1-p.ttau)/(R-p.ttau))/c;
		lhsc = ((1-p.ttau)/(R-p.ttau))/c;
		if ( (s.xxi*(kplus-bplus*(1-p.ttau)/(R-p.ttau))>=Y) &&
			 (c>0) && (kplus>0) && (n>0) && (n<=1)  ) {
			return;
		};

		// Case 1: binding case is the only possible alternative
		bindingcontrols func(s,e,p);
		double* uu = stack;
		uu[0]= s.k;
	   	uu[1] = s.b;
	   	uu[2]=p.css;
	   	uu[3]=p.dss;
	   	uu[4]=p.nss;
	   	uu[5]=p.Rss;
	   	uu[6]=p.mmuss;
	   	uu[7]=1.0;

		newt_fast(uu,func,stack);
		kplus = uu[0];
		bplus = uu[1];
		c     = uu[2];
		d     = uu[3];
		n     = uu[4];
		R     = uu[5];
		mmu   = uu[6];
		llambda=uu[7];
		lhsk = (llambda-mmu*s.xxi)/c;
		lhsb = (llambda/R-mmu*s.xxi*(1-p.ttau)/(R-p.ttau))/c;
		lhsc = ((1-p.ttau)/(R-p.ttau))/c;
		
		// Re-fill stack with zeros
		for (int i=24; i<24+64; i++) stack[i]=0.0;
	};
};
