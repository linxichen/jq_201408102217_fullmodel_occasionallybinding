#include "containers.h"
struct LUdcmp {
	// Data Member
	int n; // # of row and column of matrix
	double d;
	mat lu; // To store output L U matrix
	mat indx; // Vector stores permutation

	// Methods
	__host__ __device__ LUdcmp(const mat & A); // Constructor and performance LU dcmp
	__host__ __device__ void solve(const mat &b, mat &x); // Solve for vector x given vector b
	__host__ __device__ void inverse(mat &Ainv); // Calculate inverse of A
	__host__ __device__ double det(); // Determinant of A
	// __host__ __device__ void mprove(const mat &b, mat &x);
	// mat aref; // Used by mprove
};

__host__ __device__
LUdcmp::LUdcmp(const mat &A) {
	// Initialization
	n = A.nrows();
	lu = A;
	// aref = A;
	indx.assign(n,1,0.0);

	// Don't really know what's going on
	const double TINY=1.0e-40;
	int i,imax,j,k;
	double big,temp;
	mat vv(n,1,0.0);
	d = 1.0;
	for (i=0;i<n;i++) {
		big=0.0;
		for (j=0;j<n;j++)
			if ( (temp=abs(lu(i,j)))> big) big=temp;
		if (big == 0.0)	throw("Singular matrix in LUdcmp");
		vv(i,0)=1.0/big;
	};

	for (k=0;k<n;k++) {
		big=0.0;
		for (i=k;i<n;i++) {
			temp=vv(i)*abs(lu(i,k));
			if (temp > big) {
				big=temp;
				imax=i;
			};
		};
		if (k != imax) {
			for (j=0;j<n;j++) {
				temp=lu(imax,j);
				lu(imax,j)=lu(k,j);
				lu(k,j)=temp;
			}
			d = -d;
			vv(imax)=vv(k);
		};
		indx(k)=double(imax);
		if (lu(k,k) == 0.0) lu(k,k)=TINY;
		for (i=k+1;i<n;i++) {
			temp=lu(i,k) /= lu(k,k);
			for (j=k+1;j<n;j++)
				lu(i,j) -= temp*lu(k,j);
		};
	};
};

__host__ __device__
void LUdcmp::solve(const mat &b, mat &x)
{
	if (b.nrows() != n || x.nrows() != n || b.ncols() != x.ncols()) {
		throw("LUdcmp::solve bad sizes");
	};
	for (int i_c=0; i_c<b.ncols(); i_c++) {
		int i,ii=0,ip,j;
		double sum;
		for (i=0;i<n;i++) x(i,i_c) = b(i,i_c);
		for (i=0;i<n;i++) {
			ip=indx(i);
			sum=x(ip,i_c);
			x(ip,i_c)=x(i,i_c);
			if (ii != 0)
				for (j=ii-1;j<i;j++) sum -= lu(i,j)*x(j,i_c);
			else if (sum != 0.0)
				ii=i+1;
			x(i,i_c)=sum;
		};
		for (i=n-1;i>=0;i--) {
			sum=x(i,i_c);
			for (j=i+1;j<n;j++) sum -= lu(i,j)*x(j,i_c);
			x(i,i_c)=sum/lu(i,i);
		};
	};
};

__host__ __device__
void LUdcmp::inverse(mat &ainv)
{
	ainv.assign(n,n,0.0);
	mat eye (n,n,0.0);
	for (int i=0;i<n;i++) {
		eye(i,i) = 1.0;
	}
	solve(eye,ainv);
};

__host__ __device__
double LUdcmp::det()
{
	double dd = d;
	for (int i=0;i<n;i++) dd *= lu(i,i);
	return dd;
};

// Iterative Improvement for large system
// void LUdcmp::mprove(VecDoub_I &b, VecDoub_IO &x)
// {
// 	Int i,j;
// 	VecDoub r(n);
// 	for (i=0;i<n;i++) {
// 		Ldoub sdp = -b[i];
// 		for (j=0;j<n;j++)
// 			sdp += (Ldoub)aref[i][j] * (Ldoub)x[j];
// 		r[i]=sdp;
// 	}
// 	solve(r,r);
// 	for (i=0;i<n;i++) x[i] -= r[i];
// }

