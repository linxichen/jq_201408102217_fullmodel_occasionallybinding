#define nk 30
#define nb 30
#define nz 5
#define nxxi 5 
#define nmk 50
#define nmb 50
#define nmc 50 
#define tol 1e-7
#define maxiter 1000
#define kwidth 1.05
#define bwidth 1.05
#define mkwidth 1.7 
#define mbwidth 1.7 
#define mcwidth 1.7 

/* Includes, system */
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

// Includes, Thrust
#include <thrust/functional.h>
#include <thrust/for_each.h>
#include <thrust/sort.h>
#include <thrust/extrema.h>
#include <thrust/tuple.h>
#include <thrust/reduce.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/zip_iterator.h>

/* Includes, cuda */
#include <cuda_runtime.h>
#include <cuda.h>
#include <cublas_v2.h>
// #include <helper_functions.h>
#include "cuda_helpers.h"

// Includes, C++ codes
#include "cppcode.h"

// Includes, model specific things
#include "model.h"

using namespace std;
using namespace thrust;

// Generate Initial Guess from loglinearized solution
void guess_loglinear(const host_vector<double> K, const host_vector<double> B, const host_vector<double> Z, const host_vector<double> XXI, host_vector<double> & MK_low, host_vector<double> & MK_high,host_vector<double> & MB_low, host_vector<double> & MB_high,host_vector<double> & MC_low, host_vector<double> & MC_high, para p, double factor_low, double factor_high) {
	// Create guesses.
	for (int i_k=0; i_k<nk; i_k++) {
		for (int i_b = 0; i_b < nb; i_b++) {
			for (int i_z = 0; i_z < nz; i_z++) {
				for (int i_xxi = 0; i_xxi < nxxi; i_xxi++) {
					double temp_mk = exp(
						   			(+0.220720)
								   +(-0.000026)
						           +(+0.226584)*(log(B[i_b])-log(p.bss))
								   +(-1.193711)*(log(K[i_k])-log(p.kss))
								   +(+0.000374)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(-0.002971)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
								   +(+0.165299)*(log(B[i_b])-log(p.bss))*(log(B[i_b])-log(p.bss))
								   +(-0.254253)*(log(K[i_k])-log(p.kss))*(log(B[i_b])-log(p.bss))
								   +(-0.032497)*(log(K[i_k])-log(p.kss))*(log(K[i_k])-log(p.kss))
							       );
					MK_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_low*temp_mk;
					MK_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mk;

					double temp_mb = exp(
						   			(+0.209248)
								   +(-0.000026)
						           +(+0.257511)*(log(B[i_b])-log(p.bss))
								   +(-1.243325)*(log(K[i_k])-log(p.kss))
								   +(+0.000543)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(-0.003534)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
								   +(+0.174716)*(log(B[i_b])-log(p.bss))*(log(B[i_b])-log(p.bss))
								   +(-0.250688)*(log(K[i_k])-log(p.kss))*(log(B[i_b])-log(p.bss))
								   +(-0.054716)*(log(K[i_k])-log(p.kss))*(log(K[i_k])-log(p.kss))
							       );
					MB_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_low*temp_mb;
					MB_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mb;


					double temp_mc = exp(
						   			(+0.209269)
								   +(-0.000004)
						           +(-0.007981)*(log(B[i_b])-log(p.bss))
								   +(-0.640784)*(log(K[i_k])-log(p.kss))
								   +(-0.001435)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.000466)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
								   +(+0.013509)*(log(B[i_b])-log(p.bss))*(log(B[i_b])-log(p.bss))
								   +(-0.073250)*(log(K[i_k])-log(p.kss))*(log(B[i_b])-log(p.bss))
								   +(+0.048527)*(log(K[i_k])-log(p.kss))*(log(K[i_k])-log(p.kss))
							       );
					MC_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_low*temp_mc;
					MC_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mc;
				};
			};
		};
	};
};

struct adrian_extra {
	// Data Member, extra info needed to determine survival 
	int i_z, i_xxi;
	double *Emk_low, *Emk_high;
	double *Emb_low, *Emb_high;
	double *Emc_low, *Emc_high;
	double *K;
	double *B;
	int noisy;
	int iter;
};

__host__ __device__
bool eureka(const state& s, const shadow& m, const para& p, const adrian_extra& ae) {
	double interp_mk_low, interp_mk_high;
	double interp_mb_low, interp_mb_high;
	double interp_mc_low, interp_mc_high;
	int i_kplus, i_bplus;
	control u1;

	// Case 1: Binding 
	u1.compute(s,m,p,1);

	// A series of tests whether it make senses
	if (u1.c <= 0) {
		// printf("c=%f\n", u1.c);
		return false;
	};
	if (u1.kplus <= 0) {
		// printf("kplus=%f\n", u1.kplus);
		return false;
	};
	// if (u1.kplus < ae.K[0]) {
	// 	// printf("kplus=%f\n", u1.kplus);
	// 	return false;
	// };
	// if (u1.kplus > ae.K[nk-1]) {
	// 	// printf("kplus=%f\n", u1.kplus);
	// 	return false;
	// };
	// if (u1.bplus < ae.B[0]) {
	// 	// printf("bplus=%f\n", u1.bplus);
	// 	return false;
	// };
	// if (u1.bplus > ae.B[nb-1]) {
	// 	// printf("bplus=%f\n", u1.bplus);
	// 	return false;
	// };
	// if (u1.R <= p.ttau) {
	// 	// Interest rate has to be postive
	// 	// printf("R=%f\n", u1.R);
	// 	return false;
	// };
	// if (u1.R <= 1.0) {
	// 	// Interest rate has to be postive
	// 	// printf("R=%f\n", u1.R);
	// 	return false;
	// };
	if ( (u1.n < 0) || (u1.n > 1) ) {
		// Hours out of bound.
		// printf("n=%f\n", u1.n);
		return false;
	};

	i_kplus = fit2grid(u1.kplus,nk,ae.K);
	i_bplus = fit2grid(u1.bplus,nb,ae.B);

	// int q11 = i_kplus+i_bplus*nk+ae.iz*nk*nb+ae.i_xxi*nk*nb*nz;
	// int q21 = i_kplus+1+i_bplus*nk+ae.iz*nk*nb+ae.i_xxi*nk*nb*nz;
	// int q21 = i_kplus+(i_bplus+1)*nk+ae.iz*nk*nb+ae.i_xxi*nk*nb*nz;
	// int q21 = i_kplus+1+(i_bplus+1)*nk+ae.iz*nk*nb+ae.i_xxi*nk*nb*nz;
	double iterd = ae.iter;
	double crit = 0.5/pow(iterd,0.5);
	interp_mk_low = ae.Emk_low[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	interp_mk_high = ae.Emk_high[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	interp_mb_low = ae.Emb_low[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	interp_mb_high = ae.Emb_high[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	interp_mc_low = ae.Emc_low[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	interp_mc_high = ae.Emc_high[i_kplus+i_bplus*nk+ae.i_z*nk*nb+ae.i_xxi*nk*nb*nz];
	if (ae.iter < 300) {
		// if ( (u1.lhsk > p.bbeta*interp_mk_high) || (p.bbeta*interp_mk_low > u1.lhsk) ) {
		// 	// Euler equation for mk fails
		// 	// printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
		// 	return false;
		// };
		if ( abs(u1.lhsk - p.bbeta*(interp_mk_low+interp_mk_high)/2) > crit) {
			// Euler equation for mk fails
			// printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
			return false;
		};

		if ( abs(u1.lhsb - p.bbeta*(interp_mb_low+interp_mb_high)/2) > crit) {
			// Euler equation for mk fails
			// printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
			return false;
		};
		// if ( (u1.lhsb > p.bbeta*interp_mb_high) || (p.bbeta*interp_mb_low > u1.lhsb) ) {
		// 	// printf("lhsb=%f, rhsb_low=%f, rhsb_high=%f\n",u1.lhsb, p.bbeta*interp_mb_low, p.bbeta*interp_mb_high);
		// 	return false;
		// };

		if ( abs(u1.lhsc - p.bbeta*(interp_mc_low+interp_mc_high)/2) > crit) {
			// Euler equation for mk fails
			// printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
			return false;
		};
		// if ( (u1.lhsc > p.bbeta*interp_mc_high) || (p.bbeta*interp_mc_low > u1.lhsc) ) {
		// 	// printf("lhsc=%f, rhsc_low=%f, rhsc_high=%f\n",u1.lhsc, p.bbeta*interp_mc_low, p.bbeta*interp_mc_high);
		// 	// Euler equation for mc fails
		// 	return false;
		// };
	} else {
		if ( (u1.lhsk > p.bbeta*interp_mk_high) || (p.bbeta*interp_mk_low > u1.lhsk) ) {
			// Euler equation for mk fails
			// printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
			return false;
		};

		if ( (u1.lhsb > p.bbeta*interp_mb_high) || (p.bbeta*interp_mb_low > u1.lhsb) ) {
			// printf("lhsb=%f, rhsb_low=%f, rhsb_high=%f\n",u1.lhsb, p.bbeta*interp_mb_low, p.bbeta*interp_mb_high);
			return false;
		};

		if ( (u1.lhsc > p.bbeta*interp_mc_high) || (p.bbeta*interp_mc_low > u1.lhsc) ) {
			// printf("lhsc=%f, rhsc_low=%f, rhsc_high=%f\n",u1.lhsc, p.bbeta*interp_mc_low, p.bbeta*interp_mc_high);
			// Euler equation for mc fails
			return false;
		};
	};


	// A sensible m found, we escape the loop
	return true;
};

struct shrink 
{
	// Data member
	double *K, *B, *Z, *XXI;

	double *MK_low;
	double *MK_high;
	double *MplusK_low;
	double *MplusK_high;
	double *Emk_low;
	double *Emk_high;

	double *MB_low;
	double *MB_high;
	double *MplusB_low;
	double *MplusB_high;
	double *Emb_low;
	double *Emb_high;

	double *MC_low;
	double *MC_high;
	double *MplusC_low;
	double *MplusC_high;
	double *Emc_low;
	double *Emc_high;

	double *flag;
	double *debug;
	int    iter;
	para p;

	// Construct this object, create util from _util, etc.
	__host__ __device__
	shrink(double* K_ptr, double* B_ptr, double* Z_ptr, double* XXI_ptr,
	double *MK_low_ptr,
	double *MK_high_ptr,
	double *MplusK_low_ptr,
	double *MplusK_high_ptr,
	double *Emk_low_ptr,
	double *Emk_high_ptr,
	double *MB_low_ptr,
	double *MB_high_ptr,
	double *MplusB_low_ptr,
	double *MplusB_high_ptr,
	double *Emb_low_ptr,
	double *Emb_high_ptr,
	double *MC_low_ptr,
	double *MC_high_ptr,
	double *MplusC_low_ptr,
	double *MplusC_high_ptr,
	double *Emc_low_ptr,
	double *Emc_high_ptr,
	double* flag_ptr,
	double* debug_ptr,
	int		_iter,
	para _p)
	{
		K = K_ptr; B = B_ptr; Z = Z_ptr; XXI = XXI_ptr;

		MK_low = MK_low_ptr;
		MK_high = MK_high_ptr;
		MplusK_low = MplusK_low_ptr;
		MplusK_high = MplusK_high_ptr;
		Emk_low = Emk_low_ptr;
		Emk_high = Emk_high_ptr;

		MB_low = MB_low_ptr;
		MB_high = MB_high_ptr;
		MplusB_low = MplusB_low_ptr;
		MplusB_high = MplusB_high_ptr;
		Emb_low = Emb_low_ptr;
		Emb_high = Emb_high_ptr;
		
		MC_low = MC_low_ptr;
		MC_high = MC_high_ptr;
		MplusC_low = MplusC_low_ptr;
		MplusC_high = MplusC_high_ptr;
		Emc_low = Emc_low_ptr;
		Emc_high = Emc_high_ptr;

		flag = flag_ptr;
		debug = debug_ptr;
		iter = _iter;
		p = _p;
	};

	__host__ __device__
	void operator()(int index) {
		// Testing
		// Matrix<double> A(3,3,99);
		// printf("value=%f.\n",A(1,1));
		// A(1,1) = 10;
		// printf("value=%f.\n",A(1,1));

		// Return if not zero
		// if (index != 0) {
		// 	return;
		// };

		// Perform ind2sub
		int subs[4];
		int size_vec[4];
		size_vec[0] = nk;
		size_vec[1] = nb;
		size_vec[2] = nz;
		size_vec[3] = nxxi;
		ind2sub(4,size_vec,index,subs);
		int i_k = subs[0];
		int i_b = subs[1];
		int i_z = subs[2];
		int i_xxi = subs[3];

		// Find the "box" or "hypercube" that described m's range. Fancy word.
		double mkmin = MK_low[index]; double mkmax = MK_high[index];
		double mbmin = MB_low[index]; double mbmax = MB_high[index];
		double mcmin = MC_low[index]; double mcmax = MC_high[index];

		double mkmin_old = mkmin; double mkmax_old = mkmax;
		double mbmin_old = mbmin; double mbmax_old = mbmax;
		double mcmin_old = mcmin; double mcmax_old = mcmax;
		
		double stepk = (mkmax_old-mkmin_old)/double(nmk-1);
		double stepb = (mbmax_old-mbmin_old)/double(nmb-1);
		double stepc = (mcmax_old-mcmin_old)/double(nmc-1);

		double tempflag = 0.0;

		// Find and construct state and control, otherwise they won't update in the for loop
		double k =K[i_k]; double b = B[i_b]; double z=Z[i_z]; double xxi=XXI[i_xxi];
		state s(k,b,z,xxi,p);

		adrian_extra ae;
		ae.i_z = i_z;
		ae.i_xxi = i_xxi;
		ae.Emk_low = Emk_low;
		ae.Emk_high = Emk_high;
		ae.Emb_low = Emb_low;
		ae.Emb_high = Emb_high;
		ae.Emc_low = Emc_low;
		ae.Emc_high = Emc_high;
		ae.iter = iter;

		// Initial hunt
		int resume_ind = nmk*nmb*nmc;
		for (int m_index = 0; m_index < nmk*nmb*nmc; m_index++) {
			int i_mc = m_index/(nmk*nmb);
			int i_mb = (m_index-i_mc*nmk*nmb)/(nmk);
			int i_mk = (m_index-i_mc*nmk*nmb-i_mb*nmk);
			double mk = mkmin_old+double(i_mk)*stepk;
			double mb = mbmin_old+double(i_mb)*stepb;
			double mc = mcmin_old+double(i_mc)*stepc;
			if (eureka(s,shadow(mk,mb,mc),p,ae)) {
				mkmin = mk;
				mkmax = mk;
				mbmin = mb;
				mbmax = mb;
				mcmin = mc;
				mcmax = mc;
				resume_ind = m_index;
				tempflag++;
				// printf("mkmin=%f,mbmin=%f,mcmin=%f,mkmax=%f,mbmax=%f,mcmax=%f\n",mkmin_old,mbmin_old,mcmin_old,mkmax_old,mbmax_old,mcmax_old);
				if (index==0) {
					debug[m_index] = 1;
				};
				break;
			};
		};

		// Brute force your way through
		for (int m_index = resume_ind; m_index < nmk*nmb*nmc; m_index++) {
			int i_mc = m_index/(nmk*nmb);
			int i_mb = (m_index-i_mc*nmk*nmb)/(nmk);
			int i_mk = (m_index-i_mc*nmk*nmb-i_mb*nmk);
			double mk = mkmin_old+double(i_mk)*stepk;
			double mb = mbmin_old+double(i_mb)*stepb;
			double mc = mcmin_old+double(i_mc)*stepc;
			if (eureka(s,shadow(mk,mb,mc),p,ae)) {
				if (mk < mkmin) {
					mkmin = mk;
				};
				if (mk > mkmax) {
					mkmax = mk;
				};
				if (mb < mbmin) {
				   	mbmin = mb;
				};
				if (mb > mbmax) {
					mbmax = mb;
				};
				if (mc < mcmin) {
					mcmin = mc;
				};
				if (mc > mcmax) {
					mcmax = mc;
				};
				tempflag++;
				if (index==0) {
					debug[m_index] = 1;
				};
			};
		};

		// Update Vs
		flag[index] = double(tempflag)/double(nmk*nmb*nmc);
		if (tempflag != 0) {
			MplusK_high[index] = mkmax + int(mkmax_old - mkmax >= stepk)*stepk; 
			MplusK_low[index]  = mkmin - int(mkmin - mkmin_old >= stepk)*stepk; 

			MplusB_high[index] = mbmax + int(mbmax_old - mbmax >= stepb)*stepb; 
			MplusB_low[index]  = mbmin - int(mbmin - mbmin_old >= stepb)*stepb; 

			MplusC_high[index] = mcmax + int(mcmax_old - mcmax >= stepc)*stepc; 
			MplusC_low[index]  = mcmin - int(mcmin - mcmin_old >= stepc)*stepc; 

			// MplusK_high[index] = mkmax;
			// MplusK_low[index]  = mkmin;

			// MplusB_high[index] = mbmax;
			// MplusB_low[index]  = mbmin;

			// MplusC_high[index] = mcmax;
			// MplusC_low[index]  = mcmin;
 		// } else {
		// 	MplusK_high[index] = 999;
		// 	MplusK_low[index]  =-999;

		// 	MplusB_high[index] = 999;
		// 	MplusB_low[index]  =-999; 

		// 	MplusC_high[index] = 999;
		// 	MplusC_low[index]  =-999;
		};
	};
};	

// This functor calculates the error
struct myMinus {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return max( abs(get<0>(t)-get<1>(t)),abs(get<2>(t)-get<3>(t)) );
	}
};

// This functor calculates the distance 
struct myDist {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return abs(get<0>(t)-get<1>(t));
	}
};

int main(int argc, char ** argv)
{
	// Set heap size
	cudaDeviceSetLimit(cudaLimitMallocHeapSize, 500*30000*sizeof(cuComplex));
	// Reset GPU to start fresh
	// cudaDeviceReset();

	// Initialize Parameters
	para p;

	// Set Model Parameters
	p.aalpha = 1.8834;
	p.bbeta = 0.9825;
	p.ddelta = 0.025;
	p.ttheta = 0.36;
	p.kkappa = 0.1460;
	p.ttau = 0.3500;
	p.xxibar = 0.1634;
	p.zbar = 1.0;
	p.rrhozz = 0.9457;
	p.rrhoxxiz = 0.0321;
	p.rrhozxxi =-0.0091;
	p.rrhoxxixxi = 0.9703;
	p.var_epsz = 0.0045*0.0045;
	p.var_epsxxi = 0.0098*0.0098;
	p.complete(); // complete all implied p. find S-S

	cout << setprecision(16) << "kss: " << p.kss << endl;
	cout << setprecision(16) << "bss: " << p.bss << endl;
	cout << setprecision(16) << "zss: " << p.zbar << endl;
	cout << setprecision(16) << "xxiss: " <<p.xxibar << endl;
	cout << setprecision(16) << "mkss: " << p.mkss << endl;
	cout << setprecision(16) << "mbss: " << p.mbss << endl;
	cout << setprecision(16) << "mcss: " << p.mcss << endl;
	cout << setprecision(16) << "dss: " << p.dss << endl;
	cout << setprecision(16) << "Rss: " << p.Rss << endl;
	cout << setprecision(16) << "css: " << p.css << endl;
	cout << setprecision(16) << "nss: " << p.nss << endl;
	cout << setprecision(16) << "wss: " << p.wss << endl;
	cout << setprecision(16) << "mmuss: " << p.mmuss << endl;
	cout << setprecision(16) << "aalpha: " << p.aalpha << endl;
	cout << setprecision(16) << "tol: " << tol << endl;

	// Select Device
	int num_devices;
	cudaGetDeviceCount(&num_devices);
	if (argc > 1) {
		int gpu = min(num_devices,atoi(argv[1]));
		cudaSetDevice(gpu);
	};
	bool noisy = false;
	if (argc > 2) {
		std::string argv2 = argv[2];
		if (argv2 == "noisy") noisy = true;
	};

	// Only for cuBLAS
	const double alpha = 1.0;
	const double beta = 0.0;

	// Create all STATE, SHOCK grids here
	host_vector<double> h_K(nk); 
	host_vector<double> h_B(nb); 
	host_vector<double> h_Z(nz);
	host_vector<double> h_XXI(nxxi);

	host_vector<double> h_MK_low(nk*nb*nz*nxxi, (1.0316)*0.9);
	host_vector<double> h_MK_high(nk*nb*nz*nxxi,1.5641*1.1);
	host_vector<double> h_MplusK_low = h_MK_low;
	host_vector<double> h_MplusK_high = h_MK_high;
	host_vector<double> h_Emk_low(nk*nb*nz*nxxi,2.0);
	host_vector<double> h_Emk_high(nk*nb*nz*nxxi,3.0);

	host_vector<double> h_MB_low(nk*nb*nz*nxxi, 0.9*0.7807);
	host_vector<double> h_MB_high(nk*nb*nz*nxxi,1.1*1.8522);
	host_vector<double> h_MplusB_low = h_MB_low;
	host_vector<double> h_MplusB_high = h_MB_high;
	host_vector<double> h_Emb_low(nk*nb*nz*nxxi,0.0);
	host_vector<double> h_Emb_high(nk*nb*nz*nxxi,0.0);

	host_vector<double> h_MC_low(nk*nb*nz*nxxi, 0.9*1.0357);
	host_vector<double> h_MC_high(nk*nb*nz*nxxi,1.1*1.5226);
	host_vector<double> h_MplusC_low = h_MC_low;
	host_vector<double> h_MplusC_high = h_MC_high;
	host_vector<double> h_Emc_low(nk*nb*nz*nxxi,0.0);
	host_vector<double> h_Emc_high(nk*nb*nz*nxxi,0.0);

	host_vector<double> h_P(nz*nxxi*nz*nxxi, double(1.0/(nz*nxxi)));
	host_vector<double> h_flag(nk*nb*nz*nxxi, 3); 

	host_vector<double> h_debug(nmk*nmb*nmc,0);

	// Create capital grid
	// double minK = 1/kwidth*p.kss;
	// double maxK = kwidth*p.kss;
	double minK = 0.8*p.kss;
	double maxK = 1.2*p.kss;
	linspace(minK,maxK,nk,raw_pointer_cast(h_K.data()));

	// Create bond grid
	// double minB = 1/bwidth*p.bss;
	// double maxB = bwidth*p.bss;
	double minB = 0.8*p.bss;
	double maxB = 1.2*p.bss;
	linspace(minB,maxB,nb,raw_pointer_cast(h_B.data()));

	// Create shocks grids via Tauchen
	// host_vector<double> h_shockgrids(2*nz);
	// double* h_shockgrids_ptr = raw_pointer_cast(h_shockgrids.data());
	// double* h_P_ptr = raw_pointer_cast(h_P.data());
	// gridgen_fptr linspace_fptr = &linspace; // select linspace as grid gen
	// tauchen_vec(2,nz,3,p.A,p.Ssigma_e,h_shockgrids_ptr,h_P_ptr,linspace_fptr);
	// for (int i_shock = 0; i_shock < nz; i_shock++) {
	// 	h_Z[i_shock] = p.zbar*exp(h_shockgrids[i_shock+0*nz]);
	// 	h_XXI[i_shock] = p.xxibar*exp(h_shockgrids[i_shock+1*nz]);
	// };

	// Create shocks grids iid
	linspace(0.8,1.2,nz,h_Z.data());
	linspace(0.8*p.xxibar,1.2*p.xxibar,nxxi,h_XXI.data());

	save_vec(h_K,"./MATLAB/Kgrid.csv");
	save_vec(h_B,"./MATLAB/Bgrid.csv");
	save_vec(h_Z,"./MATLAB/Zgrid.csv");
	save_vec(h_XXI,"./MATLAB/XXIgrid.csv");


	// Obtain initial guess from linear solution
	// load_vec(h_MK_low,"./MATLAB/mkguess_low.csv");
	// load_vec(h_MB_low,"./MATLAB/mbguess_low.csv");
	// load_vec(h_MC_low,"./MATLAB/mcguess_low.csv");
	// load_vec(h_MK_high,"./MATLAB/mkguess_high.csv");
	// load_vec(h_MB_high,"./MATLAB/mbguess_high.csv");
	// load_vec(h_MC_high,"./MATLAB/mcguess_high.csv");
	// load_vec(h_MplusK_low,"./MATLAB/mkguess_low.csv");
	// load_vec(h_MplusB_low,"./MATLAB/mbguess_low.csv");
	// load_vec(h_MplusC_low,"./MATLAB/mcguess_low.csv");
	// load_vec(h_MplusK_high,"./MATLAB/mkguess_high.csv");
	// load_vec(h_MplusB_high,"./MATLAB/mbguess_high.csv");
	// load_vec(h_MplusC_high,"./MATLAB/mcguess_high.csv");

	// Copy to the device
	device_vector<double> d_K = h_K;
	device_vector<double> d_B = h_B;
	device_vector<double> d_Z = h_Z;
	device_vector<double> d_XXI = h_XXI;

	device_vector<double> d_MK_low = h_MK_low;
	device_vector<double> d_MK_high = h_MK_high;
	device_vector<double> d_MplusK_low = h_MplusK_low;
	device_vector<double> d_MplusK_high = h_MplusK_high;
	device_vector<double> d_Emk_low = h_Emk_low;
	device_vector<double> d_Emk_high = h_Emk_high;

	device_vector<double> d_MB_low = h_MB_low;
	device_vector<double> d_MB_high = h_MB_high;
	device_vector<double> d_MplusB_low = h_MplusB_low;
	device_vector<double> d_MplusB_high = h_MplusB_high;
	device_vector<double> d_Emb_low = h_Emb_low;
	device_vector<double> d_Emb_high = h_Emb_high;

	device_vector<double> d_MC_low = h_MC_low;
	device_vector<double> d_MC_high = h_MC_high;
	device_vector<double> d_MplusC_low = h_MplusC_low;
	device_vector<double> d_MplusC_high = h_MplusC_high;
	device_vector<double> d_Emc_low = h_Emc_low;
	device_vector<double> d_Emc_high = h_Emc_high;

	device_vector<double> d_P = h_P;
	device_vector<double> d_flag = h_flag;
	device_vector<double> d_debug = h_debug;

	// Obtain device pointers to be used by cuBLAS
	double* d_K_ptr   = raw_pointer_cast(d_K.data());
	double* d_B_ptr   = raw_pointer_cast(d_B.data());
	double* d_Z_ptr   = raw_pointer_cast(d_Z.data());
	double* d_XXI_ptr = raw_pointer_cast(d_XXI.data());

	double* d_MK_low_ptr      = raw_pointer_cast(d_MK_low.data());
	double* d_MK_high_ptr     = raw_pointer_cast(d_MK_high.data());
	double* d_MplusK_low_ptr  = raw_pointer_cast(d_MplusK_low.data());
	double* d_MplusK_high_ptr = raw_pointer_cast(d_MplusK_high.data());
	double* d_Emk_low_ptr     = raw_pointer_cast(d_Emk_low.data());
	double* d_Emk_high_ptr    = raw_pointer_cast(d_Emk_high.data());

	double* d_MB_low_ptr      = raw_pointer_cast(d_MB_low.data());
	double* d_MB_high_ptr     = raw_pointer_cast(d_MB_high.data());
	double* d_MplusB_low_ptr  = raw_pointer_cast(d_MplusB_low.data());
	double* d_MplusB_high_ptr = raw_pointer_cast(d_MplusB_high.data());
	double* d_Emb_low_ptr     = raw_pointer_cast(d_Emb_low.data());
	double* d_Emb_high_ptr    = raw_pointer_cast(d_Emb_high.data());

	double* d_MC_low_ptr      = raw_pointer_cast(d_MC_low.data());
	double* d_MC_high_ptr     = raw_pointer_cast(d_MC_high.data());
	double* d_MplusC_low_ptr  = raw_pointer_cast(d_MplusC_low.data());
	double* d_MplusC_high_ptr = raw_pointer_cast(d_MplusC_high.data());
	double* d_Emc_low_ptr     = raw_pointer_cast(d_Emc_low.data());
	double* d_Emc_high_ptr    = raw_pointer_cast(d_Emc_high.data());

	double* d_P_ptr = raw_pointer_cast(d_P.data());
	double* d_flag_ptr = raw_pointer_cast(d_flag.data());
	double* d_debug_ptr = raw_pointer_cast(d_debug.data());

	// Firstly a virtual index array from 0 to nk*nk*nz
	counting_iterator<int> begin(0);
	counting_iterator<int> end(nk*nb*nz*nxxi);

	// Start Timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,NULL);
	
	// Step.1 Has to start with this command to create a handle
	cublasHandle_t handle;

	// Step.2 Initialize a cuBLAS context using Create function,
	// and has to be destroyed later
	cublasCreate(&handle);
	
	double diff = 10; double dist = 100; int iter = 0;
	while ( (diff>tol)&&(iter<maxiter) ){
		// Find EMs for low and high 
		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MK_low_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emk_low_ptr,
			nk*nb);
		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MK_high_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emk_high_ptr,
			nk*nb);

		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MB_low_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emb_low_ptr,
			nk*nb);
		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MB_high_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emb_high_ptr,
			nk*nb);

		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MC_low_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emc_low_ptr,
			nk*nb);
		cublasDgemm(handle,
			CUBLAS_OP_N,  
			CUBLAS_OP_T,
			nk*nb, nz*nxxi, nz*nxxi,
			&alpha,
			d_MC_high_ptr, 
			nk*nb, 
			d_P_ptr,
			nz*nxxi,
			&beta,
			d_Emc_high_ptr,
			nk*nb);
		cout << "Expectation Found!!!" << endl;

		// Shrink
		thrust::for_each(
			begin,
			end,
			shrink(d_K_ptr, d_B_ptr, d_Z_ptr, d_XXI_ptr,
				d_MK_low_ptr,
				d_MK_high_ptr,
				d_MplusK_low_ptr,
				d_MplusK_high_ptr,
				d_Emk_low_ptr,
				d_Emk_high_ptr,
				d_MB_low_ptr,
				d_MB_high_ptr,
				d_MplusB_low_ptr,
				d_MplusB_high_ptr,
				d_Emb_low_ptr,
				d_Emb_high_ptr,
				d_MC_low_ptr,
				d_MC_high_ptr,
				d_MplusC_low_ptr,
				d_MplusC_high_ptr,
				d_Emc_low_ptr,
				d_Emc_high_ptr,
				d_flag_ptr,
				d_debug_ptr,
				iter,
				p)
		);
		cout << "Shrunk!!!" << endl;

		// Find error
		double diffk = transform_reduce(
			make_zip_iterator(make_tuple(d_MK_low.begin(), d_MplusK_low.begin(), d_MK_high.begin(),d_MplusK_high.begin())),
			make_zip_iterator(make_tuple(d_MK_low.end()  , d_MplusK_low.end()  , d_MK_high.end()  ,d_MplusK_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffb = transform_reduce(
			make_zip_iterator(make_tuple(d_MB_low.begin(), d_MplusB_low.begin(), d_MB_high.begin(),d_MplusB_high.begin())),
			make_zip_iterator(make_tuple(d_MB_low.end()  , d_MplusB_low.end()  , d_MB_high.end()  ,d_MplusB_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffc = transform_reduce(
			make_zip_iterator(make_tuple(d_MC_low.begin(), d_MplusC_low.begin(), d_MC_high.begin(),d_MplusC_high.begin())),
			make_zip_iterator(make_tuple(d_MC_low.end()  , d_MplusC_low.end()  , d_MC_high.end()  ,d_MplusC_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);

		// Find distance 
		double distk = transform_reduce(
			make_zip_iterator(make_tuple(d_MplusK_low.begin(),d_MplusK_high.begin())),
			make_zip_iterator(make_tuple(d_MplusK_low.end()  ,d_MplusK_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);
		double distb = transform_reduce(
			make_zip_iterator(make_tuple(d_MplusB_low.begin(),d_MplusB_high.begin())),
			make_zip_iterator(make_tuple(d_MplusB_low.end()  ,d_MplusB_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);
		double distc = transform_reduce(
			make_zip_iterator(make_tuple(d_MplusC_low.begin(),d_MplusC_high.begin())),
			make_zip_iterator(make_tuple(d_MplusC_low.end()  ,d_MplusC_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);

		diff = max(diffk,max(diffb,diffc));
		dist = max(distk,max(distb,distc));

		// update correspondence
		d_MK_low = d_MplusK_low; d_MK_high = d_MplusK_high;
		d_MB_low = d_MplusB_low; d_MB_high = d_MplusB_high;
		d_MC_low = d_MplusC_low; d_MC_high = d_MplusC_high;
		iter++;

		// Display 
		if (noisy == true) {
			cout << "diff is: "<< diff << endl;
			cout << "dist is: "<< dist << endl;
			cout << "MplusK[0,0,0,0] (the spike) range is " << d_MplusK_low[0] << ", " << d_MplusK_high[0] << endl;
			cout << "MplusB[0,0,0,0] (the spike) range is " << d_MplusB_low[0] << ", " << d_MplusB_high[0] << endl;
			cout << "MplusC[0,0,0,0] (the spike) range is " << d_MplusC_low[0] << ", " << d_MplusC_high[0] << endl;
			cout << iter << endl;
			cout << "=====================" << endl;
		};
	};

	// Stop Timer
	cudaEventRecord(stop,NULL);
	cudaEventSynchronize(stop);
	float msecTotal = 0.0;
	cudaEventElapsedTime(&msecTotal, start, stop);


	//==========cuBLAS stuff ends=======================
	// Step.3 Destroy the handle.
	cublasDestroy(handle);

	// Compute and print the performance
	float msecPerMatrixMul = msecTotal;
	cout << "Time= " << msecPerMatrixMul << " msec, iter= " << iter << ", Dist = " << dist << endl;

	// Copy back to host and print to file
	h_MK_low = d_MK_low; h_MK_high = d_MK_high;
	h_MB_low = d_MB_low; h_MB_high = d_MB_high;
	h_MC_low = d_MC_low; h_MC_high = d_MC_high;
	h_flag = d_flag;
	
	// Compute and save the decision variables
	host_vector<double> h_copt(nk*nb*nz*nxxi);
	host_vector<double> h_kopt(nk*nb*nz*nxxi);
	host_vector<double> h_nopt(nk*nb*nz*nxxi);
	host_vector<double> h_mmuopt(nk*nb*nz*nxxi);
	host_vector<double> h_dopt(nk*nb*nz*nxxi);
	host_vector<double> h_wopt(nk*nb*nz*nxxi);
	host_vector<double> h_Ropt(nk*nb*nz*nxxi);

	for (int i_k=0; i_k<nk; i_k++) {
		for (int i_b = 0; i_b < nb; i_b++) {
			for (int i_z = 0; i_z < nz; i_z++) {
				for (int i_xxi=0; i_xxi < nxxi; i_xxi++) {
					int index = i_k+i_b*nk+i_z*nk*nb+i_xxi*nk*nz*nb;
					double mk = (h_MK_low[index]+h_MK_low[index])/2;
					double mb = (h_MB_low[index]+h_MB_low[index])/2;
					double mc = (h_MC_low[index]+h_MC_low[index])/2;
					double k = h_K[i_k]; double b = h_B[i_b];
					double z=h_Z[i_z]; double xxi=h_XXI[i_xxi];
					control u;
					state s(k,b,z,xxi,p);

					// Try not binding first
					u.compute(s,shadow(mk,mb,mc),p,1);
					h_copt[index] = u.c;
					h_kopt[index] = u.kplus;
					h_nopt[index] = u.n;
					h_mmuopt[index] = u.mmu;
					h_dopt[index] = u.d;
					h_wopt[index] = u.w;
					h_Ropt[index] = u.R;
				};
			};
		};
	};
	
	save_vec(h_K,"./adrian_results/Kgrid.csv");
	save_vec(h_Z,"./adrian_results/Zgrid.csv");
	save_vec(h_XXI,"./adrian_results/XXIgrid.csv");
	save_vec(h_P,"./adrian_results/P.csv");
	// save_vec(h_V1_low,"./adrian_results/V1_low_guess.csv");
	// save_vec(h_V1_high,"./adrian_results/V1_high_guess.csv");
	// save_vec(h_V1_low,"./adrian_results/V1_low.csv");
	// save_vec(h_V1_high,"./adrian_results/V1_high.csv");
	// save_vec(h_flag,"./adrian_results/flag.csv");
	save_vec(h_copt,"./adrian_results/copt.csv");
	save_vec(h_kopt,"./adrian_results/kopt.csv");
	save_vec(h_nopt,"./adrian_results/nopt.csv");
	save_vec(h_mmuopt,"./adrian_results/mmuopt.csv");
	save_vec(h_dopt,"./adrian_results/dopt.csv");
	save_vec(h_wopt,"./adrian_results/wopt.csv");
	save_vec(h_Ropt,"./adrian_results/Ropt.csv");
	// save_vec(h_kk_1,"./adrian_results/kk_1.csv");
	// save_vec(h_kk_2,"./adrian_results/kk_2.csv");
	// save_vec(h_nn_1,"./adrian_results/nn_1.csv");
	// save_vec(h_nn_2,"./adrian_results/nn_2.csv");
	// save_vec(h_lhs1_1,"./adrian_results/lhs1_1.csv");
	// save_vec(h_lhs1_2,"./adrian_results/lhs1_2.csv");
	// save_vec(h_rhslow_1,"./adrian_results/rhslow_1.csv");
	// save_vec(h_rhshigh_1,"./adrian_results/rhshigh_1.csv");
	// save_vec(h_rhslow_2,"./adrian_results/rhslow_2.csv");
	// save_vec(h_rhshigh_2,"./adrian_results/rhshigh_2.csv");

	h_flag = d_flag;
	save_vec(h_flag,"./adrian_results/flag.csv");
	h_debug = d_debug;
	save_vec(d_debug,"./adrian_results/debug.csv");

	// display_vec(h_flag);

	// Export parameters to MATLAB
	p.exportmatlab("./MATLAB/mypara.m");

	return 0;
};
