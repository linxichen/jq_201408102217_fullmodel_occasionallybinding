#define nk 30
#define nb 30
#define nz 5
#define nxxi 5
#define nmk 30
#define nmb 30 
#define nmc 30 
#define tol 1e-7
#define maxiter 10000
#define kwidth 1.1
#define bwidth 1.1
#define mkwidth 1.7 
#define mbwidth 1.7 
#define mcwidth 1.7 

/* Includes, system */
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

// Includes, Thrust
#include <thrust/functional.h>
#include <thrust/for_each.h>
#include <thrust/sort.h>
#include <thrust/extrema.h>
#include <thrust/tuple.h>
#include <thrust/reduce.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/zip_iterator.h>

/* Includes, cuda */
#include <cuda_runtime.h>
#include <cuda.h>
#include <cublas_v2.h>
// #include <helper_functions.h>
#include "cuda_helpers.h"

// Includes, C++ codes
#include "cppcode.h"

// Includes, model specific things
#include "model.h"

using namespace std;
using namespace thrust;

// Generate Initial Guess from loglinearized solution
void guess_loglinear(const host_vector<double> K, const host_vector<double> B, const host_vector<double> Z, const host_vector<double> XXI, host_vector<double> & MKOVERMB_low, host_vector<double> & MKOVERMB_high,host_vector<double> & MBOVERMC_low, host_vector<double> & MBOVERMC_high,host_vector<double> & MC_low, host_vector<double> & MC_high, para p, double factor_low, double factor_high) {
	// Create guesses.
	for (int i_k=0; i_k<nk; i_k++) {
		for (int i_b = 0; i_b < nb; i_b++) {
			for (int i_z = 0; i_z < nz; i_z++) {
				for (int i_xxi = 0; i_xxi < nxxi; i_xxi++) {
					double temp_mbovermc = exp(
						   			0.0	
						           +0.265492*(log(B[i_b])-log(p.bss))
								   +(-0.602541)*(log(K[i_k])-log(p.kss))
								   +(0.001979)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(-0.003999)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					MBOVERMC_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi]  = factor_low*temp_mbovermc;
					MBOVERMC_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mbovermc;

					double temp_mkovermb = exp(
						   			0.011473
						           +(-0.030926)*(log(B[i_b])-log(p.bss))
								   +(-0.049614)*(log(K[i_k])-log(p.kss))
								   +(-0.000169)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.000562)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					MKOVERMB_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi]  = 1.01*(1-p.ddelta);
					MKOVERMB_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mkovermb;

					double temp_mc = exp(
						   			log(p.mcss)
						           +(-0.007981)*(log(B[i_b])-log(p.bss))
								   +(-0.640784)*(log(K[i_k])-log(p.kss))
								   +(-0.001435)*(log(Z[i_z])-log(p.zbar))/sqrt(p.var_epsz)
								   +(+0.000466)*(log(XXI[i_xxi])-log(p.xxibar))/sqrt(p.var_epsxxi)
							       );
					MC_low[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_low*temp_mc;
					MC_high[i_k+i_b*nk+nb*nk*i_z+nb*nk*nz*i_xxi] = factor_high*temp_mc;
				};
			};
		};
	};
};

struct adrian_extra {
	// Data Member, extra info needed to determine survival 
	int i_z, i_xxi;
	double *MKOVERMB_low, *MKOVERMB_high;
	double *MBOVERMC_low, *MBOVERMC_high;
	double *MC_low, *MC_high;
	double *K;
	double *B;
	double *P;
};

__host__ __device__
bool eureka(const state s, const shadow m, control u1, const para p, const adrian_extra ae, int noisy) {
	double interp_mk_low, interp_mk_high;
	double interp_mb_low, interp_mb_high;
	double interp_mc_low, interp_mc_high;
	int i_kplus, i_bplus;

	// Case 1: Binding 
	u1.compute(s,m,p,1);

	// A series of tests whether it make senses
	if (u1.c <= 0) {
		if (noisy==1) printf("c=%f\n", u1.c);
		return false;
	};
	if (u1.kplus <= 0) {
		if (noisy==1) printf("kplus=%f\n", u1.kplus);
		return false;
	};
	if (u1.kplus < ae.K[0]) {
		if (noisy==1) printf("kplus=%f\n", u1.kplus);
		return false;
	};
	if (u1.kplus > ae.K[nk-1]) {
		if (noisy==1) printf("kplus=%f\n", u1.kplus);
		return false;
	};
	if (u1.bplus < ae.B[0]) {
		if (noisy==1) printf("bplus=%f\n", u1.bplus);
		return false;
	};
	if (u1.bplus > ae.B[nb-1]) {
		if (noisy==1) printf("bplus=%f\n", u1.bplus);
		return false;
	};
	if (u1.R < 1) {
		// Interest rate has to be postive
		if (noisy==1) printf("R=%f\n", u1.R);
		return false;
	};
	if ( (u1.n <= 0) || (u1.n >= 1) ) {
		// Hours out of bound.
		if (noisy==1) printf("n=%f\n", u1.n);
		return false;
	};
	// if (u1.d < 0) {
	// 	if (noisy==1) printf("d=%f\n", u1.d);
	// 	return false;
	// };

	i_kplus = fit2evengrid(u1.kplus,nk,ae.K[0],ae.K[nk-1]);
	i_bplus = fit2evengrid(u1.bplus,nb,ae.B[0],ae.B[nb-1]);
	int i_kplusright = min(i_kplus+1,nk-1);
	int i_bplushigh = min(i_bplus+1,nb-1);
	double k_left = ae.K[i_kplus];
	double k_right = ae.K[min(i_kplus+1,nk-1)];
	double b_left = ae.B[i_bplus];
	double b_right = ae.B[min(i_bplus+1,nb-1)];


	interp_mk_low = 0;
	interp_mk_high = 0;
	interp_mb_low = 0;
	interp_mb_high = 0;
	interp_mc_low = 0;
	interp_mc_high = 0;

	for (int i_zplus = 0; i_zplus < nz; i_zplus++) {
		for (int i_xxiplus = 0; i_xxiplus < nxxi; i_xxiplus++) {
			int P_index = ae.i_z+ae.i_xxi*nz+i_zplus*nz*nxxi+i_xxiplus*nz*nxxi*nz;

			// For all shadow values
			int M_indexleftlow   = i_kplus+i_bplus*nk+i_zplus*nk*nb+i_xxiplus*nk*nb*nz;
			int M_indexlefthigh  = i_kplus+i_bplushigh*nk+i_zplus*nk*nb+i_xxiplus*nk*nb*nz;
			int M_indexrightlow  = i_kplusright+i_bplus*nk+i_zplus*nk*nb+i_xxiplus*nk*nb*nz;
			int M_indexrighthigh = i_kplusright+i_bplushigh*nk+i_zplus*nk*nb+i_xxiplus*nk*nb*nz;

			// MC_low 
			double mc_leftlow = ae.MC_low[M_indexleftlow];
			double mc_lefthigh = ae.MC_low[M_indexlefthigh];
			double mc_rightlow = ae.MC_low[M_indexrightlow];
			double mc_righthigh = ae.MC_low[M_indexrighthigh];
			interp_mc_low += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);

			// MC_high 
			mc_leftlow = ae.MC_high[M_indexleftlow];
			mc_lefthigh = ae.MC_high[M_indexlefthigh];
			mc_rightlow = ae.MC_high[M_indexrightlow];
			mc_righthigh = ae.MC_high[M_indexrighthigh];
			interp_mc_high += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);

			// MB_low
			double mbovermc_leftlow = ae.MBOVERMC_low[M_indexleftlow];
			double mbovermc_lefthigh = ae.MBOVERMC_low[M_indexlefthigh];
			double mbovermc_rightlow = ae.MBOVERMC_low[M_indexrightlow];
			double mbovermc_righthigh = ae.MBOVERMC_low[M_indexrighthigh];
			mc_leftlow = ae.MC_low[M_indexleftlow];
			mc_lefthigh = ae.MC_low[M_indexlefthigh];
			mc_rightlow = ae.MC_low[M_indexrightlow];
			mc_righthigh = ae.MC_low[M_indexrighthigh];
			interp_mb_low += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mbovermc_leftlow, mbovermc_lefthigh,mbovermc_rightlow,mbovermc_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);

			// MB_high
			mbovermc_leftlow = ae.MBOVERMC_high[M_indexleftlow];
			mbovermc_lefthigh = ae.MBOVERMC_high[M_indexlefthigh];
			mbovermc_rightlow = ae.MBOVERMC_high[M_indexrightlow];
			mbovermc_righthigh = ae.MBOVERMC_high[M_indexrighthigh];
			mc_leftlow = ae.MC_high[M_indexleftlow];
			mc_lefthigh = ae.MC_high[M_indexlefthigh];
			mc_rightlow = ae.MC_high[M_indexrightlow];
			mc_righthigh = ae.MC_high[M_indexrighthigh];
			interp_mb_high += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mbovermc_leftlow, mbovermc_lefthigh,mbovermc_rightlow,mbovermc_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);

			// MK_low
			double mk_leftlow = ae.MKOVERMB_low[M_indexleftlow];
			double mk_lefthigh = ae.MKOVERMB_low[M_indexlefthigh];
			double mk_rightlow = ae.MKOVERMB_low[M_indexrightlow];
			double mk_righthigh = ae.MKOVERMB_low[M_indexrighthigh];
			double mb_leftlow = ae.MBOVERMC_low[M_indexleftlow];
			double mb_lefthigh = ae.MBOVERMC_low[M_indexlefthigh];
			double mb_rightlow = ae.MBOVERMC_low[M_indexrightlow];
			double mb_righthigh = ae.MBOVERMC_low[M_indexrighthigh];
			mc_leftlow = ae.MC_low[M_indexleftlow];
			mc_lefthigh = ae.MC_low[M_indexlefthigh];
			mc_rightlow = ae.MC_low[M_indexrightlow];
			mc_righthigh = ae.MC_low[M_indexrighthigh];
			interp_mk_low += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mk_leftlow, mk_lefthigh,mk_rightlow,mk_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mb_leftlow, mb_lefthigh,mb_rightlow,mb_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);

			// MK_high
			mk_leftlow = ae.MKOVERMB_high[M_indexleftlow];
			mk_lefthigh = ae.MKOVERMB_high[M_indexlefthigh];
			mk_rightlow = ae.MKOVERMB_high[M_indexrightlow];
			mk_righthigh = ae.MKOVERMB_high[M_indexrighthigh];
			mb_leftlow = ae.MBOVERMC_high[M_indexleftlow];
			mb_lefthigh = ae.MBOVERMC_high[M_indexlefthigh];
			mb_rightlow = ae.MBOVERMC_high[M_indexrightlow];
			mb_righthigh = ae.MBOVERMC_high[M_indexrighthigh];
			mc_leftlow = ae.MC_high[M_indexleftlow];
			mc_lefthigh = ae.MC_high[M_indexlefthigh];
			mc_rightlow = ae.MC_high[M_indexrightlow];
			mc_righthigh = ae.MC_high[M_indexrighthigh];
			interp_mk_high += ae.P[P_index]*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mk_leftlow, mk_lefthigh,mk_rightlow,mk_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mb_leftlow, mb_lefthigh,mb_rightlow,mb_righthigh)*
				             bilinear_interp<double>(u1.kplus,u1.bplus,k_left,k_right,b_left,b_right,mc_leftlow, mc_lefthigh,mc_rightlow,mc_righthigh);
		};
	};

	if ( (u1.lhsk > p.bbeta*interp_mk_high) || (p.bbeta*interp_mk_low > u1.lhsk) ) {
		if (noisy==1) printf("lhsk=%f, rhsk_low=%f, rhsk_high=%f\n",u1.lhsk, p.bbeta*interp_mk_low, p.bbeta*interp_mk_high);
		return false;
	};

	if ( (u1.lhsb > p.bbeta*interp_mb_high) || (p.bbeta*interp_mb_low > u1.lhsb) ) {
		if (noisy==1) printf("lhsb=%f, rhsb_low=%f, rhsb_high=%f\n",u1.lhsb, p.bbeta*interp_mb_low, p.bbeta*interp_mb_high);
		return false;
	};

	if ( (u1.lhsc > p.bbeta*interp_mc_high) || (p.bbeta*interp_mc_low > u1.lhsc) ) {
		if (noisy==1) printf("lhsc=%f, rhsc_low=%f, rhsc_high=%f\n",u1.lhsc, p.bbeta*interp_mc_low, p.bbeta*interp_mc_high);
		return false;
	};

	// A sensible m found, we escape the loop
	return true;
};

struct shrink 
{
	// Data member
	double *K, *B, *Z, *XXI;

	double *MKOVERMB_low;
	double *MKOVERMB_high;
	double *MKOVERMBplus_low;
	double *MKOVERMBplus_high;

	double *MBOVERMC_low;
	double *MBOVERMC_high;
	double *MBOVERMCplus_low;
	double *MBOVERMCplus_high;

	double *MC_low;
	double *MC_high;
	double *MplusC_low;
	double *MplusC_high;

	double *flag;
	double *P;
	int    noisy;
	para p;

	// Construct this object, create util from _util, etc.
	__host__ __device__
	shrink(double* K_ptr, double* B_ptr, double* Z_ptr, double* XXI_ptr,
	double *MKOVERMB_low_ptr,
	double *MKOVERMB_high_ptr,
	double *MKOVERMBplus_low_ptr,
	double *MKOVERMBplus_high_ptr,
	double *MBOVERMC_low_ptr,
	double *MBOVERMC_high_ptr,
	double *MBOVERMCplus_low_ptr,
	double *MBOVERMCplus_high_ptr,
	double *MC_low_ptr,
	double *MC_high_ptr,
	double *MplusC_low_ptr,
	double *MplusC_high_ptr,
	double* flag_ptr,
	double* P_ptr,
	int    _noisy,
	para _p)
	{
		K = K_ptr; B = B_ptr; Z = Z_ptr; XXI = XXI_ptr;
		MKOVERMB_low = MKOVERMB_low_ptr;
		MKOVERMB_high = MKOVERMB_high_ptr;
		MKOVERMBplus_low = MKOVERMBplus_low_ptr;
		MKOVERMBplus_high = MKOVERMBplus_high_ptr;

		MBOVERMC_low = MBOVERMC_low_ptr;
		MBOVERMC_high = MBOVERMC_high_ptr;
		MBOVERMCplus_low = MBOVERMCplus_low_ptr;
		MBOVERMCplus_high = MBOVERMCplus_high_ptr;
		
		MC_low = MC_low_ptr;
		MC_high = MC_high_ptr;
		MplusC_low = MplusC_low_ptr;
		MplusC_high = MplusC_high_ptr;

		flag = flag_ptr;
		P = P_ptr;
		noisy = _noisy;
		p = _p;
	};

	__host__ __device__
	void operator()(const int index) {
		// Perform ind2sub
		// int subs[4];
		// int size_vec[4];
		// size_vec[0] = nk;
		// size_vec[1] = nb;
		// size_vec[2] = nz;
		// size_vec[3] = nxxi;
		// ind2sub(4,size_vec,index,subs);
		// int i_k = subs[0];
		// int i_b = subs[1];
		// int i_z = subs[2];
		// int i_xxi = subs[3];
		int i_xxi = index/(nk*nb*nz);
		int i_z = (index-i_xxi*nk*nb*nz)/(nk*nb);
		int i_b = (index-i_xxi*nk*nb*nz-i_z*nk*nb)/(nk);
		int i_k = (index-i_xxi*nk*nb*nz-i_z*nk*nb-i_b*nk)/1;

		// Find the "box" or "hypercube" that described m's range. Fancy word.
		double mkovermbmin = MKOVERMB_low[index]; double mkovermbmax = MKOVERMB_high[index];
		double mbovermcmin = MBOVERMC_low[index]; double mbovermcmax = MBOVERMC_high[index];
		double mcmin = MC_low[index]; double mcmax = MC_high[index];

		double mkovermbmin_old = mkovermbmin; double mkovermbmax_old = mkovermbmax;
		double mbovermcmin_old = mbovermcmin; double mbovermcmax_old = mbovermcmax;
		double mcmin_old = mcmin; double mcmax_old = mcmax;
		
		double stepk = (mkovermbmax_old-mkovermbmin_old)/double(nmk-1);
		double stepb = (mbovermcmax_old-mbovermcmin_old)/double(nmb-1);
		double stepc = (mcmax_old-mcmin_old)/double(nmc-1);

		double tempflag = 0.0;

		// Find and construct state and control, otherwise they won't update in the for loop
		double k =K[i_k]; double b = B[i_b]; double z=Z[i_z]; double xxi=XXI[i_xxi];
		state s(k,b,z,xxi,p);
		control u1;

		adrian_extra ae;
		ae.i_z = i_z;
		ae.i_xxi = i_xxi;
		ae.MKOVERMB_low = MKOVERMB_low;
		ae.MKOVERMB_high = MKOVERMB_high;
		ae.MBOVERMC_low = MBOVERMC_low;
		ae.MBOVERMC_high = MBOVERMC_high;
		ae.MC_low = MC_low;
		ae.MC_high = MC_high;
		ae.K = K;
		ae.B = B;
		ae.P = P;

		// Initial hunt
		int resume_ind = nmk*nmb*nmc;
		for (int m_index = 0; m_index < nmk*nmb*nmc; m_index++) {
			int i_mc = m_index/(nmk*nmb);
			int i_mb = (m_index-i_mc*nmk*nmb)/(nmk);
			int i_mk = (m_index-i_mc*nmk*nmb-i_mb*nmk);
			double mkovermb = mkovermbmin_old+double(i_mk)*stepk;
			double mbovermc = mbovermcmin_old+double(i_mb)*stepb;
			double mc = mcmin_old+double(i_mc)*stepc;
			if (index==29+24*nk+0*nk*nb+3*nk*nb*nz) {
				control u2;
				shadow mm(mkovermb,mbovermc,mc,1);
				u2.compute(s,mm,p,1);
				// printf("mmu=%f,R=%f,c=%f,d=%f,n=%f,kplus=%f,bplus=%f,lhsk=%f,lhsb=%f,lhsc=%f\n",u2.mmu,u2.R,u2.c,u2.d,u2.n,u2.kplus,u2.bplus,u2.lhsk,u2.lhsb,u2.lhsc);
			};
			if (eureka(s,shadow(mkovermb,mbovermc,mc,1),u1,p,ae,noisy)) {
				mkovermbmin = mkovermb;
				mkovermbmax = mkovermb;
				mbovermcmin = mbovermc;
				mbovermcmax = mbovermc;
				mcmin = mc;
				mcmax = mc;
				resume_ind = m_index;
				tempflag++;
				break;
			};
		};

		// Brute force your way through
		for (int m_index = resume_ind; m_index < nmk*nmb*nmc; m_index++) {
			int i_mc = m_index/(nmk*nmb);
			int i_mb = (m_index-i_mc*nmk*nmb)/(nmk);
			int i_mk = (m_index-i_mc*nmk*nmb-i_mb*nmk);
			double mkovermb = mkovermbmin_old+double(i_mk)*stepk;
			double mbovermc = mbovermcmin_old+double(i_mb)*stepb;
			double mc = mcmin_old+double(i_mc)*stepc;
			if (index==29+24*nk+0*nk*nb+3*nk*nb*nz) {
				shadow mm(mkovermb,mbovermc,mc,1);
				control u2;
				u2.compute(s,mm,p,1);
				// printf("mmu=%f,R=%f,c=%f,d=%f,n=%f,kplus=%f,bplus=%f,lhsk=%f,lhsb=%f,lhsc=%f\n",u2.mmu,u2.R,u2.c,u2.d,u2.n,u2.kplus,u2.bplus,u2.lhsk,u2.lhsb,u2.lhsc);
			};
			if (eureka(s,shadow(mkovermb,mbovermc,mc,1),u1,p,ae,noisy)) {
				if (mkovermb < mkovermbmin) {
					mkovermbmin = mkovermb;
				};
				if (mkovermb > mkovermbmax) {
				   	mkovermbmax = mkovermb;
				};
				if (mbovermc < mbovermcmin) {
				   	mbovermcmin = mbovermc;
				};
				if (mbovermc > mbovermcmax) {
				   	mbovermcmax = mbovermc;
				};
				if (mc < mcmin) {
				   	mcmin = mc;
				};
				if (mc > mcmax) {
					mcmax = mc;
				};
				tempflag++;
			};
		};

		// Update Vs
		flag[index] = double(tempflag)/double(nmk*nmb*nmc);
		if (tempflag != 0) {
			MKOVERMBplus_high[index] = mkovermbmax + (mkovermbmax < mkovermbmax_old)*stepk; 
			MKOVERMBplus_low[index]  = mkovermbmin - (mkovermbmin > mkovermbmin_old)*stepk; 

			MBOVERMCplus_high[index] = mbovermcmax + (mbovermcmax < mbovermcmax_old)*stepb; 
			MBOVERMCplus_low[index]  = mbovermcmin - (mbovermcmin > mbovermcmin_old)*stepb; 

			MplusC_high[index] = mcmax + (mcmax < mcmax_old)*stepc; 
			MplusC_low[index]  = mcmin - (mcmin > mcmin_old)*stepc; 
		} else {
			MKOVERMBplus_high[index] = 999; 
			MKOVERMBplus_low[index]  =-999; 

			MBOVERMCplus_high[index] = 999; 
			MBOVERMCplus_low[index]  =-999; 

			MplusC_high[index] = 999; 
			MplusC_low[index]  =-999; 
		};
	};
};	

// This functor calculates the error
struct myMinus {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return max( abs(get<0>(t)-get<1>(t)),abs(get<2>(t)-get<3>(t)) );
	}
};

// This functor calculates the distance 
struct myDist {
	// Tuple is (V1low,Vplus1low,V1high,Vplus1high,...)
	template <typename Tuple>
	__host__ __device__
	double operator()(Tuple t)
	{
		return abs(get<0>(t)-get<1>(t));
	}
};

int main(int argc, char ** argv)
{
	// Reset GPU to start fresh
	cudaDeviceReset();

	// Initialize Parameters
	para p;

	// Set Model Parameters
	p.aalpha = 1.8834;
	p.bbeta = 0.9825;
	p.ddelta = 0.025;
	p.ttheta = 0.36;
	p.kkappa = 0.1460;
	p.ttau = 0.3500;
	p.xxibar = 0.1634;
	p.zbar = 1.0;
	p.rrhozz = 0.9457;
	p.rrhoxxiz = 0.0321;
	p.rrhozxxi =-0.0091;
	p.rrhoxxixxi = 0.9703;
	p.var_epsz = 0.0045*0.0045;
	p.var_epsxxi = 0.0098*0.0098;
	p.complete(); // complete all implied p. find S-S

	cout << setprecision(16) << "kss: " << p.kss << endl;
	cout << setprecision(16) << "bss: " << p.bss << endl;
	cout << setprecision(16) << "zss: " << p.zbar << endl;
	cout << setprecision(16) << "xxiss: " <<p.xxibar << endl;
	cout << setprecision(16) << "mkss: " << p.mkss << endl;
	cout << setprecision(16) << "mbss: " << p.mbss << endl;
	cout << setprecision(16) << "mcss: " << p.mcss << endl;
	cout << setprecision(16) << "dss: " << p.dss << endl;
	cout << setprecision(16) << "Rss: " << p.Rss << endl;
	cout << setprecision(16) << "css: " << p.css << endl;
	cout << setprecision(16) << "nss: " << p.nss << endl;
	cout << setprecision(16) << "wss: " << p.wss << endl;
	cout << setprecision(16) << "mmuss: " << p.mmuss << endl;
	cout << setprecision(16) << "aalpha: " << p.aalpha << endl;
	cout << setprecision(16) << "tol: " << tol << endl;

	// Select Device
	int num_devices;
	cudaGetDeviceCount(&num_devices);
	if (argc > 1) {
		int gpu = min(num_devices,atoi(argv[1]));
		cudaSetDevice(gpu);
	};
	bool noisy = false;
	if (argc > 2) {
		std::string argv2 = argv[2];
		if (argv2 == "noisy") noisy = true;
	};

	// Only for cuBLAS
	// const double alpha = 1.0;
	// const double beta = 0.0;

	// Create all STATE, SHOCK grids here
	host_vector<double> h_K(nk); 
	host_vector<double> h_B(nb); 
	host_vector<double> h_Z(nz);
	host_vector<double> h_XXI(nxxi);

	host_vector<double> h_MKOVERMB_low(nk*nb*nz*nxxi, 1-p.ddelta+0.0001);
	host_vector<double> h_MKOVERMB_high(nk*nb*nz*nxxi,1-p.ddelta+0.2);
	host_vector<double> h_MKOVERMBplus_low(nk*nb*nz*nxxi,-99.0);
	host_vector<double> h_MKOVERMBplus_high(nk*nb*nz*nxxi,99.0);
	// host_vector<double> h_Emk_low(nk*nb*nz*nxxi,0.0);
	// host_vector<double> h_Emk_high(nk*nb*nz*nxxi,0.0);

	host_vector<double> h_MBOVERMC_low(nk*nb*nz*nxxi, 0.9);
	host_vector<double> h_MBOVERMC_high(nk*nb*nz*nxxi,2.7);
	host_vector<double> h_MBOVERMCplus_low(nk*nb*nz*nxxi,-99.0);
	host_vector<double> h_MBOVERMCplus_high(nk*nb*nz*nxxi,99.0);
	// host_vector<double> h_Emb_low(nk*nb*nz*nxxi,0.0);
	// host_vector<double> h_Emb_high(nk*nb*nz*nxxi,0.0);

	host_vector<double> h_MC_low(nk*nb*nz*nxxi, 0.125);
	host_vector<double> h_MC_high(nk*nb*nz*nxxi,10);
	host_vector<double> h_MplusC_low(nk*nb*nz*nxxi,-99.0);
	host_vector<double> h_MplusC_high(nk*nb*nz*nxxi,99.0);
	// host_vector<double> h_Emc_low(nk*nb*nz*nxxi,0.0);
	// host_vector<double> h_Emc_high(nk*nb*nz*nxxi,0.0);

	host_vector<double> h_P(nz*nxxi*nz*nxxi, 0);
	host_vector<double> h_flag(nk*nb*nz*nxxi, 0); 

	// Create capital grid
	double minK = 9.0;
	double maxK = 11.0;
	linspace(minK,maxK,nk,raw_pointer_cast(h_K.data()));
	display_vec(h_K);

	// Create bond grid
	double minB = 2.5;
	double maxB = 4.0;
	linspace(minB,maxB,nb,raw_pointer_cast(h_B.data()));
	display_vec(h_B);

	// Create shocks grids
	host_vector<double> h_shockgrids(2*nz);
	double* h_shockgrids_ptr = raw_pointer_cast(h_shockgrids.data());
	double* h_P_ptr = raw_pointer_cast(h_P.data());
	gridgen_fptr linspace_fptr = &linspace; // select linspace as grid gen
	tauchen_vec(2,nz,4,p.A,p.Ssigma_e,h_shockgrids_ptr,h_P_ptr,linspace_fptr);
	for (int i_shock = 0; i_shock < nz; i_shock++) {
		h_Z[i_shock] = p.zbar*exp(h_shockgrids[i_shock+0*nz]);
		h_XXI[i_shock] = p.xxibar*exp(h_shockgrids[i_shock+1*nz]);
	};

	// Obtain initial guess from linear solution
	// guess_loglinear(h_K, h_B, h_Z, h_XXI, h_MKOVERMB_low, h_MKOVERMB_high,h_MBOVERMC_low, h_MBOVERMC_high,h_MC_low, h_MC_high, p, 0.1, 3.0); 
	// display_vec(h_MK_low);
		

	// Copy to the device
	device_vector<double> d_K = h_K;
	device_vector<double> d_B = h_B;
	device_vector<double> d_Z = h_Z;
	device_vector<double> d_XXI = h_XXI;

	device_vector<double> d_MKOVERMB_low = h_MKOVERMB_low;
	device_vector<double> d_MKOVERMB_high = h_MKOVERMB_high;
	device_vector<double> d_MKOVERMBplus_low = h_MKOVERMBplus_low;
	device_vector<double> d_MKOVERMBplus_high = h_MKOVERMBplus_high;
	// device_vector<double> d_Emk_low = h_Emk_low;
	// device_vector<double> d_Emk_high = h_Emk_high;

	device_vector<double> d_MBOVERMC_low = h_MBOVERMC_low;
	device_vector<double> d_MBOVERMC_high = h_MBOVERMC_high;
	device_vector<double> d_MBOVERMCplus_low = h_MBOVERMCplus_low;
	device_vector<double> d_MBOVERMCplus_high = h_MBOVERMCplus_high;
	// device_vector<double> d_Emb_low = h_Emb_low;
	// device_vector<double> d_Emb_high = h_Emb_high;

	device_vector<double> d_MC_low = h_MC_low;
	device_vector<double> d_MC_high = h_MC_high;
	device_vector<double> d_MplusC_low = h_MplusC_low;
	device_vector<double> d_MplusC_high = h_MplusC_high;
	// device_vector<double> d_Emc_low = h_Emc_low;
	// device_vector<double> d_Emc_high = h_Emc_high;

	device_vector<double> d_P = h_P;
	device_vector<double> d_flag = h_flag;

	// Obtain device pointers to be used by cuBLAS
	double* d_K_ptr   = raw_pointer_cast(d_K.data());
	double* d_B_ptr   = raw_pointer_cast(d_B.data());
	double* d_Z_ptr   = raw_pointer_cast(d_Z.data());
	double* d_XXI_ptr = raw_pointer_cast(d_XXI.data());

	double* d_MKOVERMB_low_ptr      = raw_pointer_cast(d_MKOVERMB_low.data());
	double* d_MKOVERMB_high_ptr     = raw_pointer_cast(d_MKOVERMB_high.data());
	double* d_MKOVERMBplus_low_ptr  = raw_pointer_cast(d_MKOVERMBplus_low.data());
	double* d_MKOVERMBplus_high_ptr = raw_pointer_cast(d_MKOVERMBplus_high.data());
	// double* d_Emk_low_ptr     = raw_pointer_cast(d_Emk_low.data());
	// double* d_Emk_high_ptr    = raw_pointer_cast(d_Emk_high.data());

	double* d_MBOVERMC_low_ptr      = raw_pointer_cast(d_MBOVERMC_low.data());
	double* d_MBOVERMC_high_ptr     = raw_pointer_cast(d_MBOVERMC_high.data());
	double* d_MBOVERMCplus_low_ptr  = raw_pointer_cast(d_MBOVERMCplus_low.data());
	double* d_MBOVERMCplus_high_ptr = raw_pointer_cast(d_MBOVERMCplus_high.data());
	// double* d_Emb_low_ptr     = raw_pointer_cast(d_Emb_low.data());
	// double* d_Emb_high_ptr    = raw_pointer_cast(d_Emb_high.data());

	double* d_MC_low_ptr      = raw_pointer_cast(d_MC_low.data());
	double* d_MC_high_ptr     = raw_pointer_cast(d_MC_high.data());
	double* d_MplusC_low_ptr  = raw_pointer_cast(d_MplusC_low.data());
	double* d_MplusC_high_ptr = raw_pointer_cast(d_MplusC_high.data());
	// double* d_Emc_low_ptr     = raw_pointer_cast(d_Emc_low.data());
	// double* d_Emc_high_ptr    = raw_pointer_cast(d_Emc_high.data());

	double* d_P_ptr = raw_pointer_cast(d_P.data());
	double* d_flag_ptr = raw_pointer_cast(d_flag.data());

	// Firstly a virtual index array from 0 to nk*nk*nz
	counting_iterator<int> begin(0);
	counting_iterator<int> end(nk*nb*nz*nxxi);

	// Start Timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,NULL);
	
	// Step.1 Has to start with this command to create a handle
	cublasHandle_t handle;

	// Step.2 Initialize a cuBLAS context using Create function,
	// and has to be destroyed later
	cublasCreate(&handle);
	
	double diff = 10; double dist = 100; int iter = 0;
	while ( (diff>tol)&&(iter<maxiter) ){
		// Clear the Buffer
		cudaDeviceSynchronize();

		// Find EMs for low and high 
		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MK_low_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emk_low_ptr,
		// 	nk*nb);
		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MK_high_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emk_high_ptr,
		// 	nk*nb);

		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MB_low_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emb_low_ptr,
		// 	nk*nb);
		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MB_high_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emb_high_ptr,
		// 	nk*nb);

		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MC_low_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emc_low_ptr,
		// 	nk*nb);
		// cublasDgemm(handle,
		// 	CUBLAS_OP_N,  
		// 	CUBLAS_OP_T,
		// 	nk*nb, nz*nxxi, nz*nxxi,
		// 	&alpha,
		// 	d_MC_high_ptr, 
		// 	nk*nb, 
		// 	d_P_ptr,
		// 	nz*nxxi,
		// 	&beta,
		// 	d_Emc_high_ptr,
		// 	nk*nb);
		// cout << "Expectation Found!!!" << endl;

		// Shrink
		thrust::for_each(
			begin,
			end,
			shrink(d_K_ptr, d_B_ptr, d_Z_ptr, d_XXI_ptr,
				d_MKOVERMB_low_ptr,
				d_MKOVERMB_high_ptr,
				d_MKOVERMBplus_low_ptr,
				d_MKOVERMBplus_high_ptr,
				d_MBOVERMC_low_ptr,
				d_MBOVERMC_high_ptr,
				d_MBOVERMCplus_low_ptr,
				d_MBOVERMCplus_high_ptr,
				d_MC_low_ptr,
				d_MC_high_ptr,
				d_MplusC_low_ptr,
				d_MplusC_high_ptr,
				d_flag_ptr,
				d_P_ptr,
				noisy,
				p)
		);
		cout << "Shrunk!!!" << endl;

		// Find error
		double diffk = transform_reduce(
			make_zip_iterator(make_tuple(d_MKOVERMB_low.begin(), d_MKOVERMBplus_low.begin(), d_MKOVERMB_high.begin(),d_MKOVERMBplus_high.begin())),
			make_zip_iterator(make_tuple(d_MKOVERMB_low.end()  , d_MKOVERMBplus_low.end()  , d_MKOVERMB_high.end()  ,d_MKOVERMBplus_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffb = transform_reduce(
			make_zip_iterator(make_tuple(d_MBOVERMC_low.begin(), d_MBOVERMCplus_low.begin(), d_MBOVERMC_high.begin(),d_MBOVERMCplus_high.begin())),
			make_zip_iterator(make_tuple(d_MBOVERMC_low.end()  , d_MBOVERMCplus_low.end()  , d_MBOVERMC_high.end()  ,d_MBOVERMCplus_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);
		double diffc = transform_reduce(
			make_zip_iterator(make_tuple(d_MC_low.begin(), d_MplusC_low.begin(), d_MC_high.begin(),d_MplusC_high.begin())),
			make_zip_iterator(make_tuple(d_MC_low.end()  , d_MplusC_low.end()  , d_MC_high.end()  ,d_MplusC_high.end())),
			myMinus(),
			0.0,
			maximum<double>()
			);

		// Find distance 
		double distk = transform_reduce(
			make_zip_iterator(make_tuple(d_MKOVERMBplus_low.begin(),d_MKOVERMBplus_high.begin())),
			make_zip_iterator(make_tuple(d_MKOVERMBplus_low.end()  ,d_MKOVERMBplus_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);
		double distb = transform_reduce(
			make_zip_iterator(make_tuple(d_MBOVERMCplus_low.begin(),d_MBOVERMCplus_high.begin())),
			make_zip_iterator(make_tuple(d_MBOVERMCplus_low.end()  ,d_MBOVERMCplus_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);
		double distc = transform_reduce(
			make_zip_iterator(make_tuple(d_MplusC_low.begin(),d_MplusC_high.begin())),
			make_zip_iterator(make_tuple(d_MplusC_low.end()  ,d_MplusC_high.end())),
			myDist(),
			0.0,
			maximum<double>()
			);

		diff = max(diffk,max(diffb,diffc));
		dist = max(distk,max(distb,distc));

		// update correspondence
		d_MKOVERMB_low = d_MKOVERMBplus_low; d_MKOVERMB_high = d_MKOVERMBplus_high;
		d_MBOVERMC_low = d_MBOVERMCplus_low; d_MBOVERMC_high = d_MBOVERMCplus_high;
		d_MC_low = d_MplusC_low; d_MC_high = d_MplusC_high;
		iter++;

		// Display 
		cout << "diff is: "<< diff << endl;
		cout << "dist is: "<< dist << endl;
		cout << "distk is: "<< distk << endl;
		cout << "distb is: "<< distb << endl;
		cout << "distc is: "<< distc << endl;
		cout << "MplusK[0,0,0,0] (the spike) range is " << d_MKOVERMB_low[0] << ", " << d_MKOVERMB_high[0] << endl;
		cout << "MplusB[0,0,0,0] (the spike) range is " << d_MBOVERMC_low[0] << ", " << d_MBOVERMC_high[0] << endl;
		cout << "MplusC[0,0,0,0] (the spike) range is " << d_MC_low[0] << ", " << d_MC_high[0] << endl;
		cout << iter << endl;
		cout << "=====================" << endl;
	};

	// Stop Timer
	cudaEventRecord(stop,NULL);
	cudaEventSynchronize(stop);
	float msecTotal = 0.0;
	cudaEventElapsedTime(&msecTotal, start, stop);


	//==========cuBLAS stuff ends=======================
	// Step.3 Destroy the handle.
	cublasDestroy(handle);

	// Compute and print the performance
	float msecPerMatrixMul = msecTotal;
	cout << "Time= " << msecPerMatrixMul << " msec, iter= " << iter << ", Dist = " << dist << endl;

/* Later I will deal with this
	// Copy back to host and print to file
	h_V1_low = d_V1_low; h_V1_high = d_V1_high;
	h_EM1_low = d_EM1_low; h_EM1_high = d_EM1_high;
	h_flag = d_flag;
	
	// Compute and save the decision variables
	host_vector<double> h_copt(nk*nz*nxxi);
	host_vector<double> h_kopt(nk*nz*nxxi);
	host_vector<double> h_nopt(nk*nz*nxxi);
	host_vector<double> h_mmuopt(nk*nz*nxxi);
	host_vector<double> h_dopt(nk*nz*nxxi);
	host_vector<double> h_wopt(nk*nz*nxxi);
	host_vector<double> h_kk_1(nm1);
	host_vector<double> h_kk_2(nm1);
	host_vector<double> h_lhs1_1(nm1);
	host_vector<double> h_lhs1_2(nm1);
	host_vector<double> h_rhslow_1(nm1);
	host_vector<double> h_rhshigh_1(nm1);
	host_vector<double> h_rhslow_2(nm1);
	host_vector<double> h_rhshigh_2(nm1);
	host_vector<double> h_nn_1(nm1);
	host_vector<double> h_nn_2(nm1);

	for (int i_k=0; i_k<nk; i_k++) {
		for (int i_z = 0; i_z < nz; i_z++) {
			for (int i_xxi=0; i_xxi < nxxi; i_xxi++) {
				int index = i_k+i_z*nk+i_xxi*nk*nz;
				double m1 = (h_V1_high[index]+h_V1_high[index])/2;
				double k = h_K[i_k];
				double z=h_Z[i_z]; double xxi=h_XXI[i_xxi];
				control u;
				state s(k,z,xxi,p);

				// Try not binding first
				u.compute(s,shadow(m1),p,0);
				if (
						(s.xxi*u.kplus > u.Y) &&
						(u.c > 0) && 
						(u.kplus > 0) &&
						(u.n > 0) &&
						(u.n < 1) 
				   )
				{
					h_copt[index] = u.c;
					h_kopt[index] = u.kplus;
					h_nopt[index] = u.n;
					h_mmuopt[index] = u.mmu;
					h_dopt[index] = u.d;
					h_wopt[index] = u.w;
				} else {
					u.compute(s,shadow(m1),p,1);
					h_copt[index] = u.c;
					h_kopt[index] = u.kplus;
					h_nopt[index] = u.n;
					h_mmuopt[index] = u.mmu;
					h_dopt[index] = u.d;
					h_wopt[index] = u.w;
				};

			};
		};
	};
	
	save_vec(h_K,nk,"./adrian_results/Kgrid.csv");
	save_vec(h_Z,"./adrian_results/Zgrid.csv");
	save_vec(h_XXI,"./adrian_results/XXIgrid.csv");
	save_vec(h_P,"./adrian_results/P.csv");
	save_vec(h_V1_low,"./adrian_results/V1_low_guess.csv");
	save_vec(h_V1_high,"./adrian_results/V1_high_guess.csv");
	save_vec(h_V1_low,"./adrian_results/V1_low.csv");
	save_vec(h_V1_high,"./adrian_results/V1_high.csv");
	save_vec(h_flag,"./adrian_results/flag.csv");
	save_vec(h_copt,"./adrian_results/copt.csv");
	save_vec(h_kopt,"./adrian_results/kopt.csv");
	save_vec(h_nopt,"./adrian_results/nopt.csv");
	save_vec(h_mmuopt,"./adrian_results/mmuopt.csv");
	save_vec(h_dopt,"./adrian_results/dopt.csv");
	save_vec(h_wopt,"./adrian_results/wopt.csv");
	save_vec(h_kk_1,"./adrian_results/kk_1.csv");
	save_vec(h_kk_2,"./adrian_results/kk_2.csv");
	save_vec(h_nn_1,"./adrian_results/nn_1.csv");
	save_vec(h_nn_2,"./adrian_results/nn_2.csv");
	save_vec(h_lhs1_1,"./adrian_results/lhs1_1.csv");
	save_vec(h_rhshigh_1,"./adrian_results/rhshigh_1.csv");
	save_vec(h_rhslow_2,"./adrian_results/rhslow_2.csv");
	save_vec(h_rhshigh_2,"./adrian_results/rhshigh_2.csv");
*/
	h_MC_low = d_MC_low;
	h_MC_high = d_MC_high;
	h_MBOVERMC_low = d_MBOVERMC_low;
	h_MBOVERMC_high = d_MBOVERMC_high;
	h_flag = d_flag;

	save_vec(h_K,"./adrian_results/Kgrid.csv");
	save_vec(h_B,"./adrian_results/Bgrid.csv");
	save_vec(h_Z,"./adrian_results/Zgrid.csv");
	save_vec(h_XXI,"./adrian_results/XXIgrid.csv");
	save_vec(h_MC_low,"./adrian_results/MC_low.csv");
	save_vec(h_MC_high,"./adrian_results/MC_high.csv");
	save_vec(h_MBOVERMC_low,"./adrian_results/MBOVERMC_low.csv");
	save_vec(h_MBOVERMC_high,"./adrian_results/MBOVERMC_high.csv");
	save_vec(h_flag,"./adrian_results/flag.csv");

	// display_vec(h_flag);

	// Export parameters to MATLAB
	p.exportmatlab("./MATLAB/mypara.m");

	return 0;
}
